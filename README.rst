Silhouette
==========

Silhouette is a camera app that can apply multiple filters to video frames, and
creates virtual camera devices so that other applications such as browsers can
capture the filtered video in the same way as they capture video from real
camera devices.

It uses `GStreamer <https://gstreamer.freedesktop.org>`_ for video processing,
`PipeWire <https://pipewire.org>`_ to create the virtual camera devices, and
`GTK <https://www.gtk.org>`_ for the UI.
