/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include <unistd.h>

#include "logger.h"
#include "pipeline.h"

enum {
  PIPELINE_PROP_0,
  PIPELINE_PROP_NAME,
  PIPELINE_PROP_G_MAIN_CONTEXT,
};

enum {
  PIPELINE_SIGNAL_END_OF_STREAM,
  PIPELINE_SIGNAL_ERROR,
  PIPELINE_SIGNAL_STARTED,
  PIPELINE_NUM_SIGNALS
};

static guint32 signals [PIPELINE_NUM_SIGNALS];

typedef struct _SilPipelinePrivate SilPipelinePrivate;
struct _SilPipelinePrivate
{
  /* Props */
  gint id;
  gchar *name;
  GMainContext *g_main_context;

  GstElement *gst_pipeline;
  GSource *bus_watch_source;
  gboolean is_built;
};

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (SilPipeline, sil_pipeline, G_TYPE_OBJECT)

static gint
get_next_id ()
{
  static gint next_id = -1;
  g_atomic_int_inc (&next_id);
  return next_id;
}

static gboolean
sil_pipeline_bus_watch_cb (GstBus * bus, GstMessage * message, gpointer data)
{
  SilPipeline *self = SIL_PIPELINE (data);
  SilPipelinePrivate *priv = sil_pipeline_get_instance_private (self);

  switch (GST_MESSAGE_TYPE (message)) {
    case GST_MESSAGE_EOS:
      sil_log_info_object (self, "End of stream received on pipeline '%s'",
          priv->name);
      g_signal_emit (self, signals[PIPELINE_SIGNAL_END_OF_STREAM], 0);
      break;

    case GST_MESSAGE_ERROR: {
      GError *error;
      gchar  *debug;
      gst_message_parse_error (message, &error, &debug);
      sil_log_error_object (self, "Error received on pipeline '%s': %s",
          priv->name, error->message);
      g_signal_emit (self, signals[PIPELINE_SIGNAL_ERROR], 0, error->message);
      g_error_free (error);
      g_free (debug);
      break;
    }

    case GST_MESSAGE_STATE_CHANGED: {
      if (GST_ELEMENT (message->src) == priv->gst_pipeline) {
        GstState old = GST_STATE_VOID_PENDING, curr = GST_STATE_VOID_PENDING;
        const gchar *old_str, *curr_str;
        g_autofree gchar *file_name = NULL;

        gst_message_parse_state_changed (message, &old, &curr, NULL);
        old_str = gst_element_state_get_name (old);
        curr_str = gst_element_state_get_name (curr);

        sil_log_info_object (self,
            "State changed from %s to %s on pipeline '%s'", old_str, curr_str,
            priv->name);

        /* Emit signals */
        switch (curr) {
          case GST_STATE_PLAYING:
            g_signal_emit (self, signals[PIPELINE_SIGNAL_STARTED], 0);
            break;
          default:
            break;
        }

        /* Generate DOT graph if GST_DEBUG_DUMP_DOT_DIR is set */
        file_name = g_strdup_printf ("%s_%s_%s", priv->name, old_str, curr_str);
        gst_debug_bin_to_dot_file_with_ts (GST_BIN (priv->gst_pipeline),
            GST_DEBUG_GRAPH_SHOW_ALL, file_name);
      }
      break;
    }

    default:
      sil_log_debug_object (self,
          "New '%s' message received on pipeline '%s' from source %p",
          GST_MESSAGE_TYPE_NAME (message), priv->name, message->src);
      break;
  }

  return G_SOURCE_CONTINUE;
}

static void
sil_pipeline_init (SilPipeline * self)
{
  SilPipelinePrivate *priv = sil_pipeline_get_instance_private (self);
  priv->id = get_next_id ();
}

static void
sil_pipeline_constructed (GObject *object)
{
  SilPipeline *self = SIL_PIPELINE (object);
  SilPipelinePrivate *priv = sil_pipeline_get_instance_private (self);
  GstBus *bus;

  /* Make sure we always have a valid name */
  if (!priv->name)
    priv->name = g_strdup_printf ("silhouette-pipeline-%d-%d", getpid (),
        priv->id);

  /* Create pipeline and its bus watch source */
  priv->gst_pipeline = gst_pipeline_new (priv->name);
  bus = gst_pipeline_get_bus (GST_PIPELINE (priv->gst_pipeline));
  priv->bus_watch_source = gst_bus_create_watch (bus);
  g_clear_object (&bus);

  /* Set bus watch callback and attach to main context */
  g_source_set_callback (priv->bus_watch_source,
      G_SOURCE_FUNC (sil_pipeline_bus_watch_cb), self, NULL);
  g_source_attach (priv->bus_watch_source, priv->g_main_context);

  G_OBJECT_CLASS (sil_pipeline_parent_class)->constructed (object);
}

static void
sil_pipeline_set_property (GObject * object, guint property_id,
    const GValue * value, GParamSpec * pspec)
{
  SilPipeline *self = SIL_PIPELINE (object);
  SilPipelinePrivate *priv = sil_pipeline_get_instance_private (self);

  switch (property_id) {
    case PIPELINE_PROP_NAME:
      g_clear_pointer (&priv->name, g_free);
      priv->name = g_value_dup_string (value);
      break;

    case PIPELINE_PROP_G_MAIN_CONTEXT:
      g_clear_pointer (&priv->g_main_context, g_main_context_unref);
      priv->g_main_context = g_value_dup_boxed (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

static void
sil_pipeline_get_property (GObject * object, guint property_id, GValue * value,
    GParamSpec * pspec)
{
  SilPipeline *self = SIL_PIPELINE (object);
  SilPipelinePrivate *priv = sil_pipeline_get_instance_private (self);

  switch (property_id) {
    case PIPELINE_PROP_NAME:
      g_value_set_string (value, priv->name);
      break;

    case PIPELINE_PROP_G_MAIN_CONTEXT:
      g_value_set_boxed (value, priv->g_main_context);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

static void
sil_pipeline_dispose (GObject * object)
{
  SilPipeline *self = SIL_PIPELINE (object);

  /* Stop the pipeline */
  sil_pipeline_stop (self);
}

static void
sil_pipeline_finalize (GObject * object)
{
  SilPipeline *self = SIL_PIPELINE (object);
  SilPipelinePrivate *priv = sil_pipeline_get_instance_private (self);

  /* Destroy and clear the bus watch source */
  if (priv->bus_watch_source)
    g_source_destroy (priv->bus_watch_source);
  g_clear_pointer (&priv->bus_watch_source, g_source_unref);

  g_clear_object (&priv->gst_pipeline);

  g_clear_pointer (&priv->g_main_context, g_main_context_unref);
  g_clear_pointer (&priv->name, g_free);

  G_OBJECT_CLASS (sil_pipeline_parent_class)->finalize (object);
}

static void
sil_pipeline_class_init (SilPipelineClass * klass)
{
  GObjectClass * object_class = (GObjectClass *) klass;

  object_class->constructed = sil_pipeline_constructed;
  object_class->dispose = sil_pipeline_dispose;
  object_class->finalize = sil_pipeline_finalize;
  object_class->get_property = sil_pipeline_get_property;
  object_class->set_property = sil_pipeline_set_property;

  g_object_class_install_property (object_class, PIPELINE_PROP_NAME,
      g_param_spec_string ("name", "name", "The pipeline name", "",
          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (object_class, PIPELINE_PROP_G_MAIN_CONTEXT,
      g_param_spec_boxed ("g-main-context", "g-main-context",
          "The main context where the pipeline will be attached to",
          G_TYPE_MAIN_CONTEXT,
          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  signals[PIPELINE_SIGNAL_END_OF_STREAM] = g_signal_new ("end-of-stream",
      G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (SilPipelineClass, end_of_stream), NULL, NULL, NULL,
      G_TYPE_NONE, 0);
  signals[PIPELINE_SIGNAL_ERROR] = g_signal_new ("error",
      G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (SilPipelineClass, error), NULL, NULL, NULL,
      G_TYPE_NONE, 1, G_TYPE_STRING);
  signals[PIPELINE_SIGNAL_STARTED] = g_signal_new ("started",
      G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (SilPipelineClass, started), NULL, NULL, NULL,
      G_TYPE_NONE, 0);
}

const gchar *
sil_pipeline_get_name (SilPipeline * self)
{
  SilPipelinePrivate *priv;

  g_return_val_if_fail (SIL_IS_PIPELINE (self), NULL);

  priv = sil_pipeline_get_instance_private (self);
  return priv->name;
}

gboolean
sil_pipeline_build (SilPipeline * self)
{
  SilPipelinePrivate *priv;

  g_return_val_if_fail (SIL_IS_PIPELINE (self), FALSE);
  g_return_val_if_fail (SIL_PIPELINE_GET_CLASS (self)->build, FALSE);

  priv = sil_pipeline_get_instance_private (self);

  if (!priv->is_built) {
    priv->is_built = SIL_PIPELINE_GET_CLASS (self)->build (self,
        priv->gst_pipeline);
    if (!priv->is_built) {
      sil_log_error_object (self, "Failed to build pipeline '%s'", priv->name);
      return FALSE;
    }
  }

  return TRUE;
}

gboolean
sil_pipeline_play (SilPipeline * self)
{
  SilPipelinePrivate *priv;

  g_return_val_if_fail (SIL_IS_PIPELINE (self), FALSE);
  g_return_val_if_fail (SIL_PIPELINE_GET_CLASS (self)->build, FALSE);

  priv = sil_pipeline_get_instance_private (self);

  /* Make sure the pipeline is built */
  if (!sil_pipeline_build (self))
    return FALSE;

  switch (gst_element_set_state (priv->gst_pipeline, GST_STATE_PLAYING)) {
    case GST_STATE_CHANGE_FAILURE:
      sil_log_error_object (self, "Failed to play pipeline '%s'", priv->name);
      return FALSE;
    default:
      break;
  }

  if (SIL_PIPELINE_GET_CLASS (self)->play)
    return SIL_PIPELINE_GET_CLASS (self)->play (self);
  return TRUE;
}

void
sil_pipeline_stop (SilPipeline * self)
{
  SilPipelinePrivate *priv;

  g_return_if_fail (SIL_IS_PIPELINE (self));

  priv = sil_pipeline_get_instance_private (self);

  gst_element_set_state (priv->gst_pipeline, GST_STATE_NULL);

  if (SIL_PIPELINE_GET_CLASS (self)->stop)
    SIL_PIPELINE_GET_CLASS (self)->stop (self);
}

gboolean
sil_pipeline_is_playing (SilPipeline * self)
{
  SilPipelinePrivate *priv;
  GstState curr = GST_STATE_VOID_PENDING;
  GstStateChangeReturn scr;

  g_return_val_if_fail (SIL_IS_PIPELINE (self), FALSE);

  priv = sil_pipeline_get_instance_private (self);

  scr = gst_element_get_state (priv->gst_pipeline, &curr, NULL,
      GST_CLOCK_TIME_NONE);
  switch (scr) {
    case GST_STATE_CHANGE_FAILURE:
      sil_log_error_object (self, "Failed to get state in pipeline '%s'",
          priv->name);
      return FALSE;
    default:
      break;
  }

  return curr == GST_STATE_PLAYING;
}
