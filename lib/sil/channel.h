/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef __SILHOUETTE_CHANNEL_H__
#define __SILHOUETTE_CHANNEL_H__

#include <gst/gst.h>

#include "pipeline.h"
#include "macros.h"

G_BEGIN_DECLS

/* SilChannel */

#define SIL_TYPE_CHANNEL (sil_channel_get_type ())
SIL_API
G_DECLARE_FINAL_TYPE (SilChannel, sil_channel, SIL, CHANNEL, SilPipeline)

SIL_API
SilChannel * sil_channel_new (GMainContext *ctx, const gchar *remote_name,
    const gchar * name, const gchar *filter_factory, const gchar *input_path);

SIL_API
SilFilter * sil_channel_get_filter (SilChannel *self);

SIL_API
const gchar *sil_channel_get_input_name (SilChannel * self);

SIL_API
const gchar *sil_channel_get_source_name (SilChannel * self);

G_END_DECLS

#endif

