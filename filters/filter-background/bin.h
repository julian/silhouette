/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef __SILHOUETTE_FILTER_BACKGROUND_BIN_H__
#define __SILHOUETTE_FILTER_BACKGROUND_BIN_H__

#include <gst/gst.h>

#include "element.h"

G_BEGIN_DECLS

#define SIL_TYPE_FILTER_BACKGROUND_BIN (sil_filter_background_bin_get_type ())
G_DECLARE_FINAL_TYPE (SilFilterBackgroundBin,
    sil_filter_background_bin, SIL, FILTER_BACKGROUND_BIN, GstBin)

SilFilterBackgroundBin * sil_filter_background_bin_new (const gchar *name,
    const gchar *model_file);

G_END_DECLS

#endif
