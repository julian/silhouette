/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include <pipewire/pipewire.h>
#include <gst/gst.h>

#include "sil.h"
#include "private/logger.h"
#include "private/filter.h"

/* Singleton logger */
SilLogger * sil_logger_ = NULL;

/* Singleton filter registry */
SilFilterRegistry * sil_filter_registry_ = NULL;

void
sil_init (SilLoggerLevel level)
{
  /* Init PipeWire */
  pw_init (NULL, NULL);

  /* Init GStreamer */
  gst_init (NULL, NULL);

  /* Create the logger */
  if (!sil_logger_)
    sil_logger_ = sil_logger_new (level);

  /* Create the filter registry (loads all filters dynamically) */
  if (!sil_filter_registry_)
    sil_filter_registry_ = sil_filter_registry_new ();
}

void
sil_deinit (void)
{
  g_clear_pointer (&sil_logger_, g_object_unref);
  g_clear_pointer (&sil_filter_registry_, g_object_unref);
  gst_deinit ();
}

const gchar *
sil_get_library_version (void)
{
  return SILHOUETTE_VERSION;
}

const gchar *
sil_get_library_api_version (void)
{
  return SILHOUETTE_API_VERSION;
}

const gchar *
sil_get_filter_dir (void)
{
  static const gchar *filter_path = NULL;
  if (!filter_path) {
    filter_path = g_getenv ("SILHOUETTE_FILTER_DIR");
    if (!filter_path)
      filter_path = SILHOUETTE_DEFAULT_FILTER_DIR;
  }
  return filter_path;
}

const gchar *
sil_get_data_dir (void)
{
  static const gchar *data_path = NULL;
  if (!data_path) {
    data_path = g_getenv ("SILHOUETTE_DATA_DIR");
    if (!data_path)
      data_path = SILHOUETTE_DEFAULT_DATA_DIR;
  }
  return data_path;
}

SilFilterRegistry *
sil_get_filter_registry (void)
{
  return sil_filter_registry_ ? g_object_ref (sil_filter_registry_) : NULL;
}
