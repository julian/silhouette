/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include <glib-2.0/gmodule.h>

#include "sil.h"
#include "logger.h"
#include "filter.h"
#include "private/filter.h"

#define SIL_FILTER_LIB_PREFIX "libsilhouette"
#define SIL_FILTER_MODULE_INIT_SYMBOL "silhouette__filter_init"

extern SilFilterRegistry * sil_filter_registry_;

typedef SilFilterFactory *(*SilFilterInitFunc) (void);

/* SilFilter */

enum {
  FILTER_PROP_0,
  FILTER_PROP_NAME,
};

typedef struct _SilFilterPrivate SilFilterPrivate;
struct _SilFilterPrivate
{
  /* Props */
  gchar *name;

  GstElement *element;
};

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (SilFilter, sil_filter, G_TYPE_OBJECT)

static void
sil_filter_init (SilFilter * self)
{
}

static void
sil_filter_set_property (GObject * object, guint property_id,
    const GValue * value, GParamSpec * pspec)
{
  SilFilter *self = SIL_FILTER (object);
  SilFilterPrivate *priv = sil_filter_get_instance_private (self);

  switch (property_id) {
  case FILTER_PROP_NAME:
    g_clear_pointer (&priv->name, g_free);
    priv->name = g_value_dup_string (value);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
sil_filter_get_property (GObject * object, guint property_id, GValue * value,
    GParamSpec * pspec)
{
  SilFilter *self = SIL_FILTER (object);
  SilFilterPrivate *priv = sil_filter_get_instance_private (self);

  switch (property_id) {
  case FILTER_PROP_NAME:
    g_value_set_string (value, priv->name);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
sil_filter_finalize (GObject * object)
{
  SilFilter *self = SIL_FILTER (object);
  SilFilterPrivate *priv = sil_filter_get_instance_private (self);

  gst_clear_object (&priv->element);
  g_clear_pointer (&priv->name, g_free);

  G_OBJECT_CLASS (sil_filter_parent_class)->finalize (object);
}

static void
sil_filter_class_init (SilFilterClass * klass)
{
  GObjectClass * object_class = (GObjectClass *) klass;

  object_class->finalize = sil_filter_finalize;
  object_class->get_property = sil_filter_get_property;
  object_class->set_property = sil_filter_set_property;

  g_object_class_install_property (object_class, FILTER_PROP_NAME,
      g_param_spec_string ("name", "name", "The filter name", "",
          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));
}

const gchar *
sil_filter_get_name (SilFilter * self)
{
  g_return_val_if_fail (SIL_IS_FILTER (self), NULL);

  SilFilterPrivate *priv = sil_filter_get_instance_private (self);
  return priv->name;
}

GstElement *
sil_filter_get_element (SilFilter * self)
{
  SilFilterPrivate *priv;

  g_return_val_if_fail (SIL_IS_FILTER (self), NULL);
  g_return_val_if_fail (SIL_FILTER_GET_CLASS (self)->create_element, NULL);

  priv = sil_filter_get_instance_private (self);

  if (priv->element)
    return gst_object_ref (priv->element);

  priv->element = SIL_FILTER_GET_CLASS (self)->create_element (self);
  return gst_object_ref (priv->element);
}


/* SilFilterFactory */

enum {
  FILTER_FACTORY_PROP_0,
  FILTER_FACTORY_PROP_NAME,
};

struct _SilFilterFactory
{
  GObject parent;

  /* Props */
  GQuark name_quark;

  GType type;
};

G_DEFINE_TYPE (SilFilterFactory, sil_filter_factory, G_TYPE_OBJECT)

static void
sil_filter_factory_set_property (GObject * object, guint property_id,
    const GValue * value, GParamSpec * pspec)
{
  SilFilterFactory *self = SIL_FILTER_FACTORY (object);

  switch (property_id) {
  case FILTER_FACTORY_PROP_NAME:
    self->name_quark = g_quark_from_string (g_value_get_string (value));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
sil_filter_factory_get_property (GObject * object, guint property_id,
    GValue * value, GParamSpec * pspec)
{
  SilFilterFactory *self = SIL_FILTER_FACTORY (object);

  switch (property_id) {
  case FILTER_FACTORY_PROP_NAME:
    g_value_set_string (value, g_quark_to_string (self->name_quark));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
sil_filter_factory_init (SilFilterFactory * self)
{
}

static void
sil_filter_factory_class_init (SilFilterFactoryClass * klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  object_class->set_property = sil_filter_factory_set_property;
  object_class->get_property = sil_filter_factory_get_property;

  g_object_class_install_property (object_class, FILTER_FACTORY_PROP_NAME,
      g_param_spec_string ("name", "name", "The factory's name", "",
          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));
}

SilFilterFactory *
sil_filter_factory_new (const gchar * factory_name, GType type)
{
  SilFilterFactory *self;

  g_return_val_if_fail (factory_name != NULL, NULL);
  g_return_val_if_fail (g_type_is_a (type, SIL_TYPE_FILTER), NULL);

  self = g_object_new (SIL_TYPE_FILTER_FACTORY, NULL);
  self->name_quark = g_quark_from_static_string (factory_name);
  self->type = type;

  return self;
}

const gchar *
sil_filter_factory_get_description (SilFilterFactory *self)
{
  SilFilterClass *filter_class;
  const gchar *desc;

  g_return_val_if_fail (SIL_IS_FILTER_FACTORY (self), NULL);

  filter_class = g_type_class_ref (self->type);
  desc = filter_class->description;
  g_type_class_unref (filter_class);

  return desc;
}

SilFilter *
sil_filter_factory_construct (SilFilterFactory *self, const gchar *name)
{
  g_return_val_if_fail (SIL_IS_FILTER_FACTORY (self), NULL);

  return g_object_new (self->type, "name", name, NULL);
}

SilFilter *
sil_filter_factory_make (const gchar * factory_name, const gchar * name)
{
  g_autoptr (SilFilterFactory) factory = NULL;

  g_return_val_if_fail (factory_name != NULL, NULL);

  if (!sil_filter_registry_)
    return NULL;

  factory = sil_filter_registry_get_factory (sil_filter_registry_,
      factory_name);
  if (!factory) {
    sil_log_warn_object (sil_filter_registry_,
        "Filter factory '%s' does not exist", factory_name);
    return NULL;
  }

  return sil_filter_factory_construct (factory, name);
}


/* SilFilterRegistry */

struct _SilFilterRegistry
{
  GObject parent;

  GHashTable *factories;
};

G_DEFINE_TYPE (SilFilterRegistry, sil_filter_registry, G_TYPE_OBJECT)

static void
sil_filter_registry_init (SilFilterRegistry * self)
{
  const gchar *filter_dir = sil_get_filter_dir ();
  g_autoptr (GError) error = NULL;
  g_autoptr (GDir) dir = NULL;

  self->factories = g_hash_table_new_full (g_str_hash, g_str_equal, g_free,
      g_object_unref);

  /* Dynamically load all filters and add them into the factories table */
  dir = g_dir_open (filter_dir, 0, &error);
  if (dir) {
    const gchar *filename;
    while ((filename = g_dir_read_name (dir))) {
      g_autofree gchar *filter_path = NULL;
      GModule *module;
      gpointer filter_module_init;
      g_autoptr (SilFilterFactory) factory = NULL;
      g_autofree gchar *factory_name = NULL;

      /* Only load silhouette filter libraries */
      if (!g_str_has_prefix (filename, SIL_FILTER_LIB_PREFIX) ||
          !g_str_has_suffix (filename, G_MODULE_SUFFIX))
        continue;

      /* build the filter path */
      filter_path = g_build_filename (filter_dir, filename, NULL);
      module = g_module_open (filter_path, G_MODULE_BIND_LOCAL);
      if (!module) {
        sil_log_warn_object (self,
            "Failed to open filter module %s, ignoring...", filter_path);
        continue;
      }

      /* Get the filter module init symbol */
      if (!g_module_symbol (module, SIL_FILTER_MODULE_INIT_SYMBOL,
          &filter_module_init)) {
        sil_log_warn_object (self,
            "Filter module %s does not have init symbol, ignoring...",
            filter_path);
        g_module_close (module);
        continue;
      }

      /* Init the filter module */
      factory = ((SilFilterInitFunc) filter_module_init)();
      if (!factory) {
        sil_log_warn_object (self,
            "Filter module %s failed to initialize, ignoring...", filter_path);
        g_module_close (module);
        continue;
      }

      /* Check if the factory is already registered */
      g_object_get (factory, "name", &factory_name, NULL);
      if (g_hash_table_contains (self->factories, factory_name)) {
        sil_log_warn_object (self,
            "Factory '%s' is already registered, ignoring...", factory_name);
        g_module_close (module);
        continue;
      }

      /* Insert the factory in the factories table */
      g_hash_table_insert (self->factories, g_strdup (factory_name),
          g_steal_pointer (&factory));
      sil_log_debug_object (self,
          "Factory '%s' registered successfully", factory_name);
    }
  } else {
    sil_log_error_object (self,
        "Failed to open '%s' filter directory: %s", filter_dir, error->message);
  }
}

static void
sil_filter_registry_finalize (GObject * object)
{
  SilFilterRegistry *self = SIL_FILTER_REGISTRY (object);

  g_clear_pointer (&self->factories, g_hash_table_unref);

  G_OBJECT_CLASS (sil_filter_registry_parent_class)->finalize (object);
}

static void
sil_filter_registry_class_init (SilFilterRegistryClass * klass)
{
  GObjectClass * object_class = (GObjectClass *) klass;

  object_class->finalize = sil_filter_registry_finalize;
}

SilFilterRegistry *
sil_filter_registry_new (void)
{
  return g_object_new (SIL_TYPE_FILTER_REGISTRY, NULL);
}

SilFilterFactory *
sil_filter_registry_get_factory (SilFilterRegistry *self,
    const gchar *factory_name)
{
  SilFilterFactory *factory = NULL;

  g_return_val_if_fail (SIL_IS_FILTER_REGISTRY (self), NULL);

  factory = g_hash_table_lookup (self->factories, factory_name);
  return factory ? g_object_ref (factory) : NULL;
}

GList *
sil_filter_registry_get_factories (SilFilterRegistry *self)
{
  g_return_val_if_fail (SIL_IS_FILTER_REGISTRY (self), NULL);

  return g_hash_table_get_values (self->factories);
}
