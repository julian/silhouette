/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef __SILHOUETTE_PRIVATE_FILTER_H__
#define __SILHOUETTE_PRIVATE_FILTER_H__

#include <gst/gst.h>

G_BEGIN_DECLS

SilFilterRegistry * sil_filter_registry_new (void);

G_END_DECLS

#endif

