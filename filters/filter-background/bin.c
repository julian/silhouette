/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include <gst/pbutils/pbutils.h>

#include <sil/sil.h>

#include "bin.h"

GST_DEBUG_CATEGORY_STATIC (sil_filter_background_bin_debug);
#define GST_CAT_DEFAULT sil_filter_background_bin_debug

static GstStaticPadTemplate sil_filter_background_bin_sink_template =
    GST_STATIC_PAD_TEMPLATE ("sink",
        GST_PAD_SINK,
        GST_PAD_ALWAYS,
        GST_STATIC_CAPS ("ANY")
    );

static GstStaticPadTemplate sil_filter_background_bin_src_template =
    GST_STATIC_PAD_TEMPLATE ("src",
        GST_PAD_SRC,
        GST_PAD_ALWAYS,
        GST_STATIC_CAPS ("ANY")
    );

enum {
  PROP_0,
  PROP_MODEL_FILE,
  PROP_COLOR,
};

struct _SilFilterBackgroundBin
{
  GstBin parent;

  /* Props */
  gchar *model_file;
  SilFilterBackgroundElementColor color;

  GstElement *pre_videoconvert;
  GstElement *pre_videoscale;
  GstElement *filter;
  GstElement *post_videoscale;
  GstElement *post_videoconvert;

  const gchar *missing_element;
  gboolean constructed;
};

G_DEFINE_TYPE (SilFilterBackgroundBin, sil_filter_background_bin, GST_TYPE_BIN);

static gboolean
sil_filter_background_bin_open (SilFilterBackgroundBin * self)
{
  if (self->missing_element) {
    gst_element_post_message (GST_ELEMENT (self),
        gst_missing_element_message_new (GST_ELEMENT (self),
            self->missing_element));
  } else if (!self->constructed) {
    GST_ELEMENT_ERROR (self, CORE, FAILED,
        ("Failed to construct silhouette filter background pipeline."), (NULL));
  }

  return self->constructed;
}

static GstStateChangeReturn
sil_filter_background_bin_change_state (GstElement * element,
    GstStateChange transition)
{
  SilFilterBackgroundBin *self = SIL_FILTER_BACKGROUND_BIN (element);

  switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
      if (!sil_filter_background_bin_open (self))
        return GST_STATE_CHANGE_FAILURE;
      break;
    default:
      break;
  }

  return GST_ELEMENT_CLASS (
      sil_filter_background_bin_parent_class)->change_state (element,
          transition);
}

static void
sil_filter_background_bin_constructed (GObject * o)
{
  SilFilterBackgroundBin *self = SIL_FILTER_BACKGROUND_BIN (o);
  GstElementClass *klass = GST_ELEMENT_GET_CLASS (self);
  GstPad *src_gpad, *sink_gpad;
  GstPad *src_pad = NULL, *sink_pad = NULL;
  const gchar *data_dir = sil_get_data_dir ();

  /* Setup ghost pads */
  sink_gpad = gst_ghost_pad_new_no_target_from_template ("sink",
      gst_element_class_get_pad_template (klass, "sink"));
  gst_element_add_pad (GST_ELEMENT (self), sink_gpad);

  src_gpad = gst_ghost_pad_new_no_target_from_template ("src",
      gst_element_class_get_pad_template (klass, "src"));
  gst_element_add_pad (GST_ELEMENT (self), src_gpad);

  /* Create elements */
  self->pre_videoconvert = gst_element_factory_make ("videoconvert", NULL);
  if (!self->pre_videoconvert) {
    self->missing_element = "videoconvert";
    goto error;
  }

  self->pre_videoscale = gst_element_factory_make ("videoscale", NULL);
  if (!self->pre_videoscale) {
    self->missing_element = "videoscale";
    goto error;
  }

  self->filter = sil_filter_background_element_new (NULL);

  self->post_videoscale = gst_element_factory_make ("videoscale", NULL);
  if (!self->post_videoscale) {
    self->missing_element = "videoscale";
    goto error;
  }

  self->post_videoconvert = gst_element_factory_make ("videoconvert", NULL);
  if (!self->post_videoconvert) {
    self->missing_element = "videoconvert";
    goto error;
  }

  /* Configure the filter with the ONNX model */
  {
    g_autofree gchar *onnx_model = g_build_filename (
        data_dir, "onnx_models", "selfie_segmentation_general.onnx", NULL);
    GST_INFO_OBJECT (self, "Using ONNX model '%s'", onnx_model);
    g_object_set (self->filter, "model-file", onnx_model, NULL);
  }

  /* Add elements */
  gst_bin_add_many (GST_BIN (self),
      gst_object_ref (self->pre_videoconvert),
      gst_object_ref (self->pre_videoscale),
      gst_object_ref (self->filter),
      gst_object_ref (self->post_videoscale),
      gst_object_ref (self->post_videoconvert),
      NULL);

  /* Link elements */
  sink_pad = gst_element_get_static_pad (self->pre_videoconvert, "sink");
  gst_ghost_pad_set_target (GST_GHOST_PAD (sink_gpad), sink_pad);
  gst_clear_object (&sink_pad);

  gst_element_link_many (self->pre_videoconvert, self->pre_videoscale,
      self->filter, self->post_videoscale, self->post_videoconvert, NULL);

  src_pad = gst_element_get_static_pad (self->post_videoconvert, "src");
  gst_ghost_pad_set_target (GST_GHOST_PAD (src_gpad), src_pad);
  gst_object_unref (src_pad);

  /* Signal success */
  self->constructed = TRUE;

done:
  G_OBJECT_CLASS (sil_filter_background_bin_parent_class)->constructed (o);
  return;

error:
  gst_clear_object (&self->pre_videoconvert);
  gst_clear_object (&self->pre_videoscale);
  gst_clear_object (&self->filter);
  gst_clear_object (&self->post_videoscale);
  gst_clear_object (&self->post_videoconvert);

  self->constructed = TRUE;
  goto done;
}

static void
sil_filter_background_bin_set_property (GObject * o, guint property_id,
    const GValue * value, GParamSpec * pspec)
{
  SilFilterBackgroundBin *self = SIL_FILTER_BACKGROUND_BIN (o);

  switch (property_id) {
    case PROP_MODEL_FILE:
      g_clear_pointer (&self->model_file, g_free);
      self->model_file = g_value_dup_string (value);
      break;

    case PROP_COLOR:
      self->color = g_value_get_enum (value);
      if (self->filter)
        g_object_set (self->filter, "color", self->color, NULL);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (o, property_id, pspec);
      break;
  }
}

static void
sil_filter_background_bin_get_property (GObject * o, guint property_id,
    GValue * value, GParamSpec * pspec)
{
  SilFilterBackgroundBin *self = SIL_FILTER_BACKGROUND_BIN (o);

  switch (property_id) {
    case PROP_MODEL_FILE:
      g_value_set_string (value, self->model_file);
      break;

    case PROP_COLOR:
      g_value_set_enum (value, self->color);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (o, property_id, pspec);
      break;
  }
}

static void
sil_filter_background_bin_finalize (GObject * o)
{
  SilFilterBackgroundBin *self = SIL_FILTER_BACKGROUND_BIN (o);

  gst_clear_object (&self->pre_videoconvert);
  gst_clear_object (&self->pre_videoscale);
  gst_clear_object (&self->filter);
  gst_clear_object (&self->post_videoscale);
  gst_clear_object (&self->post_videoconvert);

  g_clear_pointer (&self->model_file, g_free);

  G_OBJECT_CLASS (sil_filter_background_bin_parent_class)->finalize (o);
}

static void
sil_filter_background_bin_init (SilFilterBackgroundBin * self)
{
  self->color = SIL_FILTER_BACKGROUND_ELEMENT_COLOR_DEFAULT;
}

static void
sil_filter_background_bin_class_init (SilFilterBackgroundBinClass * klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);

  GST_DEBUG_CATEGORY_INIT (sil_filter_background_bin_debug,
      "sil-filter-background-bin", 0, "Silhouette Filter Background Bin");

  object_class->constructed = sil_filter_background_bin_constructed;
  object_class->finalize = sil_filter_background_bin_finalize;
  object_class->set_property = sil_filter_background_bin_set_property;
  object_class->get_property = sil_filter_background_bin_get_property;

  gst_element_class_add_static_pad_template (element_class,
      &sil_filter_background_bin_src_template);
  gst_element_class_add_static_pad_template (element_class,
      &sil_filter_background_bin_sink_template);

  element_class->change_state =
      GST_DEBUG_FUNCPTR (sil_filter_background_bin_change_state);

  g_object_class_install_property (object_class, PROP_MODEL_FILE,
      g_param_spec_string ("model-file", "model-file",
          "The ONNX model file of the filter", NULL,
          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_COLOR,
      g_param_spec_enum ("color", "Color", "The background color",
          SIL_TYPE_FILTER_BACKGROUND_ELEMENT_COLOR,
          SIL_FILTER_BACKGROUND_ELEMENT_COLOR_DEFAULT,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
}

SilFilterBackgroundBin *
sil_filter_background_bin_new (const gchar *name, const gchar *model_file)
{
  return g_object_new (SIL_TYPE_FILTER_BACKGROUND_BIN,
      "name", name,
      "model-file", model_file,
      NULL);
}
