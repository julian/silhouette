/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef __SILHOUETTE_PIPELINE_H__
#define __SILHOUETTE_PIPELINE_H__

#include <gst/gst.h>

#include "macros.h"

G_BEGIN_DECLS

/* SilPipeline */

#define SIL_TYPE_PIPELINE (sil_pipeline_get_type ())
SIL_API
G_DECLARE_DERIVABLE_TYPE (SilPipeline, sil_pipeline, SIL, PIPELINE, GObject)

struct _SilPipelineClass
{
  GObjectClass parent_class;

  /* Signals */
  void (*end_of_stream) (SilPipeline * self);
  void (*error) (SilPipeline * self, const gchar *message);
  void (*started) (SilPipeline * self);

  /* Virtual methods */
  gboolean (*build) (SilPipeline * self, GstElement *gst_pipeline);
  gboolean (*play) (SilPipeline * self);
  void (*stop) (SilPipeline * self);
};

SIL_API
const gchar * sil_pipeline_get_name (SilPipeline * self);

SIL_API
gboolean sil_pipeline_build (SilPipeline * self);

SIL_API
gboolean sil_pipeline_play (SilPipeline * self);

SIL_API
void sil_pipeline_stop (SilPipeline * self);

SIL_API
gboolean sil_pipeline_is_playing (SilPipeline * self);

G_END_DECLS

#endif
