all:
	ninja -C build

install:
	ninja -C build install

uninstall:
	ninja -C build uninstall

clean:
	ninja -C build clean

run: all
	SILHOUETTE_DEBUG=3 \
	SILHOUETTE_DATA_DIR=data \
	SILHOUETTE_FILTER_DIR=build/filters \
	$(DBG) ./build/src/silhouette

test:
	meson test -C build

gdb:
	$(MAKE) run DBG=gdb
