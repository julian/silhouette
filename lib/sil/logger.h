/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef __SILHOUETTE_LOGGER_H__
#define __SILHOUETTE_LOGGER_H__

#include "macros.h"
#include "silenums.h"

G_BEGIN_DECLS

typedef enum {
  SIL_LOGGER_LEVEL_NONE = 0,
  SIL_LOGGER_LEVEL_ERROR,
  SIL_LOGGER_LEVEL_WARN,
  SIL_LOGGER_LEVEL_INFO,
  SIL_LOGGER_LEVEL_DEBUG,
  SIL_LOGGER_LEVEL_TRACE
} SilLoggerLevel;

SIL_API
void sil_logv (GObject *object, SilLoggerLevel level, const gchar *fmt,
    va_list args);

SIL_API
void sil_log (GObject *object, SilLoggerLevel level, const gchar *fmt, ...);

#define sil_log_error(F, ...) \
    sil_log (NULL, SIL_LOGGER_LEVEL_ERROR, (F), ##__VA_ARGS__)
#define sil_log_warn(F, ...) \
    sil_log (NULL, SIL_LOGGER_LEVEL_WARN, (F), ##__VA_ARGS__)
#define sil_log_info(F, ...) \
    sil_log (NULL, SIL_LOGGER_LEVEL_INFO, (F), ##__VA_ARGS__)
#define sil_log_debug(F, ...) \
    sil_log (NULL, SIL_LOGGER_LEVEL_DEBUG, (F), ##__VA_ARGS__)
#define sil_log_trace(F, ...) \
    sil_log (NULL, SIL_LOGGER_LEVEL_TRACE, (F), ##__VA_ARGS__)

#define sil_log_error_object(O, F, ...) \
    sil_log (G_OBJECT ((O)), SIL_LOGGER_LEVEL_ERROR, (F), ##__VA_ARGS__)
#define sil_log_warn_object(O, F, ...) \
    sil_log (G_OBJECT ((O)), SIL_LOGGER_LEVEL_WARN, (F), ##__VA_ARGS__)
#define sil_log_info_object(O, F, ...) \
    sil_log (G_OBJECT ((O)), SIL_LOGGER_LEVEL_INFO, (F), ##__VA_ARGS__)
#define sil_log_debug_object(O, F, ...) \
    sil_log (G_OBJECT ((O)), SIL_LOGGER_LEVEL_DEBUG, (F), ##__VA_ARGS__)
#define sil_log_trace_object(O, F, ...) \
    sil_log (G_OBJECT ((O)), SIL_LOGGER_LEVEL_TRACE, (F), ##__VA_ARGS__)

G_END_DECLS

#endif
