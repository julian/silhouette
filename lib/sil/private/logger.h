/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef __SILHOUETTE_PRIVATE_LOGGER_H__
#define __SILHOUETTE_PRIVATE_LOGGER_H__

#include <gst/gst.h>

#include "macros.h"
#include "../logger.h"

G_BEGIN_DECLS

#define SIL_TYPE_LOGGER (sil_logger_get_type ())
G_DECLARE_FINAL_TYPE (SilLogger, sil_logger, SIL, LOGGER, GObject)

SilLogger * sil_logger_new (SilLoggerLevel level);

G_END_DECLS

#endif

