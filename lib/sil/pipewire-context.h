/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef __SILHOUETTE_PIPEWIRE_CONTEXT_H__
#define __SILHOUETTE_PIPEWIRE_CONTEXT_H__

#include <gst/gst.h>
#include <pipewire/pipewire.h>

#include "macros.h"

G_BEGIN_DECLS

/* SilPipewireContext */

#define SIL_TYPE_PIPEWIRE_CONTEXT (sil_pipewire_context_get_type ())
SIL_API
G_DECLARE_DERIVABLE_TYPE (SilPipewireContext, sil_pipewire_context, SIL,
    PIPEWIRE_CONTEXT, GObject)

struct _SilPipewireContextClass
{
  GObjectClass parent_class;

  /* Signals */
  void (*core_done) (SilPipewireContext * self, guint id, gint seq);
  void (*core_error) (SilPipewireContext * self, guint id, gint seq, gint res,
      const gchar *message);

  /* Virtual methods */
  gboolean (*connect) (SilPipewireContext * self, struct pw_core *core);
  void (*disconnect) (SilPipewireContext * self);
};

SIL_API
gboolean sil_pipewire_context_connect (SilPipewireContext *self);

SIL_API
void sil_pipewire_context_disconnect (SilPipewireContext *self);

SIL_API
gboolean sil_pipewire_context_is_connected (SilPipewireContext *self);

SIL_API
gint sil_pipewire_context_sync (SilPipewireContext *self);

/* For sub-classes only */

SIL_API
void sil_pipewire_context_lock (SilPipewireContext *self);

SIL_API
void sil_pipewire_context_unlock (SilPipewireContext *self);

G_END_DECLS

#endif

