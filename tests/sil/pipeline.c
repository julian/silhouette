/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include <sil/sil.h>


/* SilFakePipeline */

struct _SilFakePipeline
{
  SilPipeline parent;
};

#define SIL_TYPE_FAKE_PIPELINE (sil_fake_pipeline_get_type ())
G_DECLARE_FINAL_TYPE (SilFakePipeline, sil_fake_pipeline, SIL, FAKE_PIPELINE,
    SilPipeline)
G_DEFINE_TYPE (SilFakePipeline, sil_fake_pipeline, SIL_TYPE_PIPELINE)

static void
sil_fake_pipeline_init (SilFakePipeline * self)
{
}

static gboolean
sil_fake_pipeline_build (SilPipeline * self, GstElement *gst_pipeline)
{
  GstElement *videotestsrc;
  GstElement *fakevideosink;

  videotestsrc = gst_element_factory_make ("videotestsrc", NULL);
  if (!videotestsrc) {
    sil_log_error_object (self,
        "Failed to create videotestsrc GStreamer element");
    goto error;
  }

  fakevideosink = gst_element_factory_make ("fakevideosink", NULL);
  if (!fakevideosink) {
    sil_log_error_object (self,
        "Failed to create fakevideosink GStreamer element");
    goto error;
  }

  gst_bin_add_many (GST_BIN (gst_pipeline), videotestsrc, fakevideosink, NULL);
  gst_element_link_many (videotestsrc, fakevideosink, NULL);

  return TRUE;

error:
  gst_clear_object (&videotestsrc);
  gst_clear_object (&fakevideosink);
  return FALSE;
}

static void
sil_fake_pipeline_class_init (SilFakePipelineClass * klass)
{
  SilPipelineClass * pipeline_class = (SilPipelineClass *) klass;

  pipeline_class->build = sil_fake_pipeline_build;
}

static SilPipeline *
sil_fake_pipeline_new (GMainContext *ctx, const gchar *name)
{
  return g_object_new (SIL_TYPE_FAKE_PIPELINE,
      "g-main-context", ctx,
      "name", name,
      NULL);
}


/* Tests */

typedef struct {
  GMainLoop *loop;
  gboolean eos;
  gboolean error;
  gboolean started;
} TestFixture;

static void
test_pipeline_setup (TestFixture *self, gconstpointer data)
{
  self->loop = g_main_loop_new (NULL, FALSE);
}

static void
test_pipeline_teardown (TestFixture *self, gconstpointer data)
{
  g_main_loop_unref (self->loop);
}

static void
on_fake_pipeline_eos (SilPipeline * p, TestFixture * f)
{
  f->eos = TRUE;
  g_main_loop_quit (f->loop);
}

static void
on_fake_pipeline_error (SilPipeline * p, const gchar *msg, TestFixture * f)
{
  f->error = TRUE;
  g_main_loop_quit (f->loop);
}

static void
on_fake_pipeline_started (SilPipeline * p, TestFixture * f)
{
  f->started = TRUE;
  g_main_loop_quit (f->loop);
}

static void
test_pipeline_basic (TestFixture *f, gconstpointer data)
{
  g_autoptr (SilPipeline) p = sil_fake_pipeline_new (NULL, "fake-pipeline");
  g_assert_nonnull (p);

  g_assert_cmpstr (sil_pipeline_get_name (p), ==, "fake-pipeline");

  g_signal_connect (p, "end-of-stream", G_CALLBACK (on_fake_pipeline_eos), f);
  g_signal_connect (p, "error", G_CALLBACK (on_fake_pipeline_error), f);
  g_signal_connect (p, "started", G_CALLBACK (on_fake_pipeline_started), f);

  /* Play pipeline */
  g_assert_true (sil_pipeline_play (p));
  g_main_loop_run (f->loop);
  g_assert_false (f->eos);
  g_assert_false (f->error);
  g_assert_true (f->started);
  g_assert_true (sil_pipeline_is_playing (p));
  f->started = FALSE;

  /* Stop the pipeline */
  sil_pipeline_stop (p);
  g_assert_false (sil_pipeline_is_playing (p));
  g_assert_false (f->eos);
  g_assert_false (f->error);
  g_assert_false (f->started);

  /* Play again pipeline */
  g_assert_true (sil_pipeline_play (p));
  g_main_loop_run (f->loop);
  g_assert_false (f->eos);
  g_assert_false (f->error);
  g_assert_true (f->started);
  g_assert_true (sil_pipeline_is_playing (p));
  f->started = FALSE;
}

gint
main (gint argc, gchar *argv[])
{
  gint res;

  g_test_init (&argc, &argv, NULL);
  sil_init (SIL_LOGGER_LEVEL_TRACE);

  g_test_add ("/sil/pipeline/basic", TestFixture, NULL,
      test_pipeline_setup, test_pipeline_basic, test_pipeline_teardown);

  res = g_test_run ();

  sil_deinit ();
  return res;
}
