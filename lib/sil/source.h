/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef __SILHOUETTE_SOURCE_H__
#define __SILHOUETTE_SOURCE_H__

#include <gst/gst.h>

#include "stream.h"
#include "macros.h"

#define SIL_SOURCE_DEFAULT_WIDTH 640
#define SIL_SOURCE_DEFAULT_HEIGHT 480
#define SIL_SOURCE_DEFAULT_FPS_N 30
#define SIL_SOURCE_DEFAULT_FPS_D 1

G_BEGIN_DECLS

/* SilSource */

#define SIL_TYPE_SOURCE (sil_source_get_type ())
SIL_API
G_DECLARE_FINAL_TYPE (SilSource, sil_source, SIL, SOURCE, SilStream)

SIL_API
SilSource * sil_source_new (const gchar *remote_name, const gchar * name,
    const gchar *link_group);

SIL_API
void sil_source_queue_buffer (SilSource *self, GstBuffer *buffer);

G_END_DECLS

#endif

