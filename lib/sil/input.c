/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include <spa/param/video/format-utils.h>
#include <pipewire/pipewire.h>
#include <gst/app/gstappsrc.h>
#include <gst/video/video-format.h>

#include "logger.h"
#include "input.h"

#define MIN_WIDTH 1
#define MIN_HEIGHT 1
#define MAX_WIDTH 16384
#define MAX_HEIGHT 16384
#define DEFAULT_WIDTH 640
#define DEFAULT_HEIGHT 480
#define MIN_FPS_N 0
#define MIN_FPS_D 1
#define MAX_FPS_N 120
#define MAX_FPS_D 1
#define DEFAULT_FPS_N 30
#define DEFAULT_FPS_D 1

enum {
  INPUT_PROP_0,
  INPUT_PROP_PATH,
};

struct _SilInput
{
  SilStream parent;

  /* Props */
  gchar *path;

  struct spa_video_info_raw spa_format;
  GWeakRef appsrc;
};

G_DEFINE_TYPE (SilInput, sil_input, SIL_TYPE_STREAM)

static GstVideoFormat
parse_spa_video_format (enum spa_video_format format)
{
  switch (format) {
    case SPA_VIDEO_FORMAT_RGB:
      return GST_VIDEO_FORMAT_RGB;
    case SPA_VIDEO_FORMAT_BGR:
      return GST_VIDEO_FORMAT_BGR;
    case SPA_VIDEO_FORMAT_RGBx:
      return GST_VIDEO_FORMAT_RGBx;
    case SPA_VIDEO_FORMAT_BGRx:
      return GST_VIDEO_FORMAT_BGRx;
    case SPA_VIDEO_FORMAT_xRGB:
      return GST_VIDEO_FORMAT_xRGB;
    case SPA_VIDEO_FORMAT_xBGR:
      return GST_VIDEO_FORMAT_xBGR;
    case SPA_VIDEO_FORMAT_RGBA:
      return GST_VIDEO_FORMAT_RGBA;
    case SPA_VIDEO_FORMAT_BGRA:
      return GST_VIDEO_FORMAT_BGRA;
    case SPA_VIDEO_FORMAT_ARGB:
      return GST_VIDEO_FORMAT_ARGB;
    case SPA_VIDEO_FORMAT_ABGR:
      return GST_VIDEO_FORMAT_ABGR;
    case SPA_VIDEO_FORMAT_YV12:
      return GST_VIDEO_FORMAT_YV12;
    case SPA_VIDEO_FORMAT_I420:
      return GST_VIDEO_FORMAT_I420;
    case SPA_VIDEO_FORMAT_NV12:
      return GST_VIDEO_FORMAT_NV12;
    case SPA_VIDEO_FORMAT_NV21:
      return GST_VIDEO_FORMAT_NV21;
    case SPA_VIDEO_FORMAT_YUY2:
      return GST_VIDEO_FORMAT_YUY2;
    case SPA_VIDEO_FORMAT_UYVY:
      return GST_VIDEO_FORMAT_UYVY;
    case SPA_VIDEO_FORMAT_YVYU:
      return GST_VIDEO_FORMAT_YVYU;
    default:
      break;
  }

  return GST_VIDEO_FORMAT_UNKNOWN;
}

static guint
get_bits_per_pixel (GstVideoFormat format)
{
  switch (format) {
    case SPA_VIDEO_FORMAT_RGB:
    case SPA_VIDEO_FORMAT_BGR:
      return 24;
    case SPA_VIDEO_FORMAT_RGBx:
    case SPA_VIDEO_FORMAT_BGRx:
    case SPA_VIDEO_FORMAT_xRGB:
    case SPA_VIDEO_FORMAT_xBGR:
    case SPA_VIDEO_FORMAT_RGBA:
    case SPA_VIDEO_FORMAT_BGRA:
    case SPA_VIDEO_FORMAT_ARGB:
    case SPA_VIDEO_FORMAT_ABGR:
      return 32;
    case SPA_VIDEO_FORMAT_YV12:
    case SPA_VIDEO_FORMAT_I420:
    case SPA_VIDEO_FORMAT_NV12:
    case SPA_VIDEO_FORMAT_NV21:
      return 12;
    case SPA_VIDEO_FORMAT_YUY2:
    case SPA_VIDEO_FORMAT_UYVY:
    case SPA_VIDEO_FORMAT_YVYU:
      return 16;
    default:
      break;
  }

  return 0;
}

static void
sil_input_param_changed (SilStream * base, guint32 id,
    const struct spa_pod *param, struct pw_stream *stream)
{
  SilInput *self = SIL_INPUT (base);

  uint8_t params_buffer[1024];
  struct spa_pod_builder b = SPA_POD_BUILDER_INIT (params_buffer,
      sizeof (params_buffer));
  const struct spa_pod *params[2];

  /* Make sure param is valid */
  if (!param)
    return;

  /* SPA_PARAM_Format */
  if (id == SPA_PARAM_Format) {
    g_autoptr (GstElement) appsrc = NULL;
    GstVideoFormat gst_format;
    guint bpp;
    gint stride;

    if (spa_format_video_raw_parse (param, &self->spa_format) < 0) {
      sil_log_warn_object (self, "Failed to parse format on input %s",
          sil_stream_get_name (base));
      return;
    }

    /* Get GStreamer format */
    gst_format = parse_spa_video_format (self->spa_format.format);
    if (gst_format == GST_VIDEO_FORMAT_UNKNOWN) {
      sil_log_warn_object (self, "Format is not supported on input %s",
          sil_stream_get_name (base));
      return;
    }

    /* Get bits per pixel */
    bpp = get_bits_per_pixel (self->spa_format.format);
    if (bpp == 0) {
      sil_log_warn_object (self, "BBP is not valid on input %s",
          sil_stream_get_name (base));
      return;
    }

    /* Calculate stride */
    stride = self->spa_format.size.width * bpp / 8;

    params[0] = spa_pod_builder_add_object (&b,
        SPA_TYPE_OBJECT_ParamBuffers, SPA_PARAM_Buffers,
        SPA_PARAM_BUFFERS_buffers, SPA_POD_CHOICE_RANGE_Int (8, 2, 64),
        SPA_PARAM_BUFFERS_blocks, SPA_POD_Int (1),
        SPA_PARAM_BUFFERS_size, SPA_POD_Int (
            stride * self->spa_format.size.height),
        SPA_PARAM_BUFFERS_stride,  SPA_POD_Int (stride),
        SPA_PARAM_BUFFERS_dataType, SPA_POD_CHOICE_FLAGS_Int(
            (1<<SPA_DATA_MemPtr)));

    params[1] = spa_pod_builder_add_object (&b,
        SPA_TYPE_OBJECT_ParamMeta, SPA_PARAM_Meta,
        SPA_PARAM_META_type, SPA_POD_Id (SPA_META_Header),
        SPA_PARAM_META_size, SPA_POD_Int (sizeof(struct spa_meta_header)));

    sil_log_info_object (self,
        "Format changed on input %s: format=%d stride=%d width=%d height=%d",
        sil_stream_get_name (base), self->spa_format.format, stride,
        self->spa_format.size.width, self->spa_format.size.height);

    /* Update appsrc caps */
    appsrc = g_weak_ref_get (&self->appsrc);
    if (appsrc) {
      GstCaps *caps = gst_caps_new_simple ("video/x-raw",
          "format", G_TYPE_STRING, gst_video_format_to_string (gst_format),
          "width", G_TYPE_INT, self->spa_format.size.width,
          "height", G_TYPE_INT, self->spa_format.size.height,
          "framerate", GST_TYPE_FRACTION,
              self->spa_format.framerate.num,
              self->spa_format.framerate.denom,
          NULL);
      gst_app_src_set_caps (GST_APP_SRC (appsrc), caps);
      gst_caps_unref (caps);
    }

    /* Update params on stream */
    pw_stream_update_params (stream, params, 2);
  }
}

static struct pw_properties *
sil_input_get_properties (SilStream * base)
{
  SilInput *self = SIL_INPUT (base);
  struct pw_properties *props;

  props = pw_properties_new (
      PW_KEY_MEDIA_TYPE, "Video",
      PW_KEY_MEDIA_CLASS, "Stream/Input/Video",
      PW_KEY_MEDIA_CATEGORY, "Capture",
      NULL);

  if (self->path)
    pw_properties_set (props, PW_KEY_TARGET_OBJECT, self->path);

  return props;
}

static gboolean
sil_input_connect_stream (SilStream * base, struct pw_stream *stream)
{
  SilInput *self = SIL_INPUT (base);
  const struct spa_pod *params[1];
  guint8 buffer[1024];
  struct spa_pod_builder b = SPA_POD_BUILDER_INIT(buffer, sizeof(buffer));

  /* Connect the stream */
  params[0] = spa_pod_builder_add_object(&b,
      SPA_TYPE_OBJECT_Format, SPA_PARAM_EnumFormat,
      SPA_FORMAT_mediaType, SPA_POD_Id (SPA_MEDIA_TYPE_video),
      SPA_FORMAT_mediaSubtype, SPA_POD_Id (SPA_MEDIA_SUBTYPE_raw),
      SPA_FORMAT_VIDEO_format, SPA_POD_CHOICE_ENUM_Id (17,
          SPA_VIDEO_FORMAT_RGB,
          SPA_VIDEO_FORMAT_BGR,
          SPA_VIDEO_FORMAT_RGBx,
          SPA_VIDEO_FORMAT_BGRx,
          SPA_VIDEO_FORMAT_xRGB,
          SPA_VIDEO_FORMAT_xBGR,
          SPA_VIDEO_FORMAT_RGBA,
          SPA_VIDEO_FORMAT_ARGB,
          SPA_VIDEO_FORMAT_BGRA,
          SPA_VIDEO_FORMAT_ABGR,
          SPA_VIDEO_FORMAT_YV12,
          SPA_VIDEO_FORMAT_I420,
          SPA_VIDEO_FORMAT_YUY2,
          SPA_VIDEO_FORMAT_UYVY,
          SPA_VIDEO_FORMAT_YVYU,
          SPA_VIDEO_FORMAT_NV12,
          SPA_VIDEO_FORMAT_NV21),
      SPA_FORMAT_VIDEO_size, SPA_POD_CHOICE_RANGE_Rectangle (
         &SPA_RECTANGLE (DEFAULT_WIDTH, DEFAULT_HEIGHT),
         &SPA_RECTANGLE (MIN_WIDTH, MIN_HEIGHT),
         &SPA_RECTANGLE (MAX_WIDTH, MAX_HEIGHT)),
      SPA_FORMAT_VIDEO_framerate, SPA_POD_CHOICE_RANGE_Fraction (
			&SPA_FRACTION (DEFAULT_FPS_N, DEFAULT_FPS_D),
			&SPA_FRACTION (MIN_FPS_N, MIN_FPS_D),
			&SPA_FRACTION (MAX_FPS_N, MAX_FPS_D)));
  if (pw_stream_connect (stream, PW_DIRECTION_INPUT, PW_ID_ANY,
      PW_STREAM_FLAG_DONT_RECONNECT |
      PW_STREAM_FLAG_AUTOCONNECT |
      PW_STREAM_FLAG_ASYNC |
      PW_STREAM_FLAG_MAP_BUFFERS,
      params, 1) < 0) {
    sil_log_error_object (self, "Failed to connect input %s",
        sil_stream_get_name (base));
    return FALSE;
  }

  sil_log_info_object (self, "Successfully connected input %s",
      sil_stream_get_name (base));
  return TRUE;
}

static gboolean
sil_input_process_buffer (SilStream * base, struct pw_buffer *b,
    struct pw_stream *stream)
{
  SilInput *self = SIL_INPUT (base);
  g_autoptr (GstElement) appsrc = NULL;

  sil_log_trace_object (self, "Processing buffer %p in input %s", b,
      sil_stream_get_name (base));

  if (!b->buffer->datas[0].data)
    return TRUE;

  appsrc = g_weak_ref_get (&self->appsrc);
  if (appsrc) {
    GstBuffer *gst_buffer;

    gst_buffer = gst_buffer_new_memdup (b->buffer->datas[0].data,
        b->buffer->datas[0].chunk->size);

    if (gst_app_src_push_buffer (GST_APP_SRC (appsrc), gst_buffer) ==
        GST_FLOW_ERROR) {
      sil_log_error_object (self,
          "Failed to push buffer %p to appsrc in input %s", b,
          sil_stream_get_name (base));
      return FALSE;
    }

    sil_log_trace_object (self, "Pushed buffer %p to appsrc in input %s", b,
      sil_stream_get_name (base));
  }

  return TRUE;
}

static void
sil_input_init (SilInput * self)
{
  g_weak_ref_init (&self->appsrc, NULL);
}

static void
sil_input_set_property (GObject * object, guint property_id,
    const GValue * value, GParamSpec * pspec)
{
  SilInput *self = SIL_INPUT (object);

  switch (property_id) {
    case INPUT_PROP_PATH:
      g_clear_pointer (&self->path, g_free);
      self->path = g_value_dup_string (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

static void
sil_input_get_property (GObject * object, guint property_id,
    GValue * value, GParamSpec * pspec)
{
  SilInput *self = SIL_INPUT (object);

  switch (property_id) {
    case INPUT_PROP_PATH:
      g_value_set_string (value, self->path);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

static void
sil_input_finalize (GObject * object)
{
  SilInput *self = SIL_INPUT (object);

  g_weak_ref_clear (&self->appsrc);

  /* Props */
  g_clear_pointer (&self->path, g_free);

  G_OBJECT_CLASS (sil_input_parent_class)->finalize (object);
}

static void
sil_input_class_init (SilInputClass * klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;
  SilStreamClass *stream_class = (SilStreamClass *) klass;

  object_class->finalize = sil_input_finalize;
  object_class->set_property = sil_input_set_property;
  object_class->get_property = sil_input_get_property;

  stream_class->param_changed = sil_input_param_changed;
  stream_class->get_properties = sil_input_get_properties;
  stream_class->connect_stream = sil_input_connect_stream;
  stream_class->process_buffer = sil_input_process_buffer;

  g_object_class_install_property (object_class, INPUT_PROP_PATH,
      g_param_spec_string ("path", "path", "The path of the input", NULL,
          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));
}

SilInput *
sil_input_new (const gchar *remote_name, const gchar * name,
    const gchar *link_group, const gchar *path)
{
  return g_object_new (SIL_TYPE_INPUT,
      "remote-name", remote_name,
      "name", name,
      "link-group", link_group,
      "path", path,
      NULL);
}

void
sil_input_set_appsrc (SilInput * self, GstElement *appsrc)
{
  g_return_if_fail (SIL_IS_INPUT (self));

  sil_pipewire_context_lock (SIL_PIPEWIRE_CONTEXT (self));
  g_weak_ref_set (&self->appsrc, appsrc);
  sil_pipewire_context_unlock (SIL_PIPEWIRE_CONTEXT (self));
}
