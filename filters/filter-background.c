/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include <sil/sil.h>

#include "filter-background/bin.h"

enum
{
  BACKGROUND_FILTER_PROP_0,
  BACKGROUND_FILTER_PROP_COLOR,
};

struct _SilBackgroundFilter
{
  SilFilter parent;

  /* Props */
  SilFilterBackgroundElementColor color;
};

#define SIL_TYPE_BACKGROUND_FILTER (sil_background_filter_get_type ())
G_DECLARE_FINAL_TYPE (SilBackgroundFilter, sil_background_filter,
                      SIL, BACKGROUND_FILTER, SilFilter)
G_DEFINE_TYPE (SilBackgroundFilter, sil_background_filter, SIL_TYPE_FILTER)

static void
sil_background_filter_init (SilBackgroundFilter * self)
{
  self->color = SIL_FILTER_BACKGROUND_ELEMENT_COLOR_DEFAULT;
}

static void
sil_background_filter_set_property (GObject * o, guint property_id,
    const GValue * value, GParamSpec * pspec)
{
  SilBackgroundFilter *self = SIL_BACKGROUND_FILTER (o);

  switch (property_id) {
    case BACKGROUND_FILTER_PROP_COLOR: {
      GstElement *element;
      self->color = g_value_get_enum (value);
      element = sil_filter_get_element (SIL_FILTER (self));
      if (element)
        g_object_set (element, "color", self->color, NULL);
      break;
    }

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (o, property_id, pspec);
      break;
  }
}

static void
sil_background_filter_get_property (GObject * o, guint property_id,
    GValue * value, GParamSpec * pspec)
{
  SilBackgroundFilter *self = SIL_BACKGROUND_FILTER (o);

  switch (property_id) {
    case BACKGROUND_FILTER_PROP_COLOR:
      g_value_set_enum (value, self->color);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (o, property_id, pspec);
      break;
  }
}

static GstElement *
sil_background_filter_create_element (SilFilter *base)
{
  return GST_ELEMENT (sil_filter_background_bin_new (NULL, NULL));
}

static void
sil_background_filter_class_init (SilBackgroundFilterClass * klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  SilFilterClass * filter_class = SIL_FILTER_CLASS (klass);

  object_class->set_property = sil_background_filter_set_property;
  object_class->get_property = sil_background_filter_get_property;

  filter_class->description = "Segments the background";
  filter_class->create_element = sil_background_filter_create_element;

  g_object_class_install_property (G_OBJECT_CLASS (klass),
      BACKGROUND_FILTER_PROP_COLOR,
      g_param_spec_enum ("color", "Color", "The background color",
          SIL_TYPE_FILTER_BACKGROUND_ELEMENT_COLOR,
          SIL_FILTER_BACKGROUND_ELEMENT_COLOR_DEFAULT,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
}

SIL_PLUGIN_EXPORT SilFilterFactory *
silhouette__filter_init (void)
{
  return sil_filter_factory_new ("background", SIL_TYPE_BACKGROUND_FILTER);
}

