/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include <pipewire/pipewire.h>

#include "logger.h"
#include "monitor.h"

enum {
  MONITOR_SIGNAL_SOURCE_ADDED,
  MONITOR_SIGNAL_SOURCE_REMOVED,
  MONITOR_NUM_SIGNALS
};

static guint32 signals [MONITOR_NUM_SIGNALS];

struct _SilMonitor
{
  SilPipewireContext parent;

  struct pw_registry *registry;
  struct spa_hook registry_listener;

  GHashTable *sources;
};

G_DEFINE_TYPE (SilMonitor, sil_monitor, SIL_TYPE_PIPEWIRE_CONTEXT)

static void
sil_monitor_init (SilMonitor * self)
{
  self->sources = g_hash_table_new_full (g_direct_hash, g_direct_equal, NULL,
      (GDestroyNotify)pw_properties_free);
}

static void
on_global (void *data, uint32_t id, uint32_t permissions,
    const char *type, uint32_t version, const struct spa_dict *properties)
{
  SilMonitor *self = SIL_MONITOR (data);

  if (g_str_equal (type, PW_TYPE_INTERFACE_Node)) {
    const gchar *media_class = spa_dict_lookup (properties, PW_KEY_MEDIA_CLASS);
    if (media_class && g_str_equal (media_class, "Video/Source")) {
      struct pw_properties *props = pw_properties_new_dict (properties);
      g_hash_table_insert (self->sources, GUINT_TO_POINTER (id), props);
      g_signal_emit (self, signals[MONITOR_SIGNAL_SOURCE_ADDED], 0, id, props);
    }
  }
}

static void
on_global_remove (void *data, uint32_t id)
{
  SilMonitor *self = SIL_MONITOR (data);
  struct pw_properties *props;

  if (self->sources) {
    if (g_hash_table_steal_extended (self->sources, GUINT_TO_POINTER (id), NULL,
        (gpointer *)&props)) {
      g_signal_emit (self, signals[MONITOR_SIGNAL_SOURCE_REMOVED], 0, id,
          props);
      pw_properties_free (props);
    }
  }
}

static const struct pw_registry_events registry_events = {
  PW_VERSION_REGISTRY_EVENTS,
  .global = on_global,
  .global_remove = on_global_remove
};

static gboolean
sil_monitor_connect (SilPipewireContext *base, struct pw_core *core)
{
  SilMonitor *self = SIL_MONITOR (base);

  /* Get registry and add listener */
  self->registry = pw_core_get_registry (core, PW_VERSION_REGISTRY, 0);
  pw_registry_add_listener (self->registry, &self->registry_listener,
      &registry_events, self);

  return TRUE;
}

static void
sil_monitor_disconnect (SilPipewireContext *base)
{
  SilMonitor *self = SIL_MONITOR (base);

  spa_hook_remove (&self->registry_listener);

  if (self->registry) {
    pw_proxy_destroy ((struct pw_proxy*)self->registry);
    self->registry = NULL;
  }
}

static void
sil_monitor_finalize (GObject * object)
{
  SilMonitor *self = SIL_MONITOR (object);

  g_clear_pointer (&self->sources, g_hash_table_unref);

  G_OBJECT_CLASS (sil_monitor_parent_class)->finalize (object);
}

static void
sil_monitor_class_init (SilMonitorClass * klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;
  SilPipewireContextClass *pc_class = (SilPipewireContextClass *) klass;

  object_class->finalize = sil_monitor_finalize;

  pc_class->connect = sil_monitor_connect;
  pc_class->disconnect = sil_monitor_disconnect;

  signals[MONITOR_SIGNAL_SOURCE_ADDED] = g_signal_new ("source-added",
      G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST,
      0, NULL, NULL, NULL,
      G_TYPE_NONE, 2, G_TYPE_UINT, G_TYPE_POINTER);
  signals[MONITOR_SIGNAL_SOURCE_REMOVED] = g_signal_new ("source-removed",
      G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST,
      0, NULL, NULL, NULL,
      G_TYPE_NONE, 2, G_TYPE_UINT, G_TYPE_POINTER);
}

SilMonitor *
sil_monitor_new (const gchar *remote_name)
{
  return g_object_new (SIL_TYPE_MONITOR,
      "remote-name", remote_name,
      NULL);
}

GArray *
sil_monitor_get_source_ids (SilMonitor *self)
{
  GArray *res;
  GPtrArray *ids;

  g_return_val_if_fail (SIL_IS_MONITOR (self), NULL);

  res = g_array_new (FALSE, FALSE, sizeof (guint32));

  ids = g_hash_table_get_keys_as_ptr_array (self->sources);
  for (guint i = 0; i < ids->len; ++i) {
    guint32 id = GPOINTER_TO_UINT (g_ptr_array_index (ids, i));
    g_array_append_val (res, id);
  }
  g_ptr_array_unref (ids);

  return res;
}

const gchar *
sil_monitor_get_source_property (SilMonitor *self, guint source_id,
    const gchar *key)
{
  struct pw_properties *props;

  g_return_val_if_fail (SIL_IS_MONITOR (self), NULL);

  props = g_hash_table_lookup (self->sources, GUINT_TO_POINTER (source_id));
  return props ? pw_properties_get (props, key) : NULL;
}
