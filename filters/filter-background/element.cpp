/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include "element.h"
#include "sil/onnx/client.hpp"

GType sil_filter_background_element_input_image_format_get_type (void);
#define SIL_TYPE_FILTER_BACKGROUND_ELEMENT_INPUT_IMAGE_FORMAT (\
    sil_filter_background_element_input_image_format_get_type ())

typedef enum {
  SIL_FILTER_BACKGROUND_ELEMENT_INPUT_IMAGE_FORMAT_HWC,
  SIL_FILTER_BACKGROUND_ELEMENT_INPUT_IMAGE_FORMAT_CHW,
} SilFilterBackgroundElementInputImageFormat;

GType
sil_filter_background_element_input_image_format_get_type (void)
{
  static GType filter_background_element_input_image_format = 0;

  if (g_once_init_enter (&filter_background_element_input_image_format)) {
    static GEnumValue filter_background_element_input_image_format_types[] = {
      {SIL_FILTER_BACKGROUND_ELEMENT_INPUT_IMAGE_FORMAT_HWC,
          "Height Width Channel (HWC)", "hwc"},
      {SIL_FILTER_BACKGROUND_ELEMENT_INPUT_IMAGE_FORMAT_CHW,
          "Channel Height Width (CHW)", "chw"},
      {0, NULL, NULL},
    };

    GType temp = g_enum_register_static (
        "SilFilterBackgroundElementInputImageFormat",
        filter_background_element_input_image_format_types);

    g_once_init_leave (&filter_background_element_input_image_format, temp);
  }

  return filter_background_element_input_image_format;
}

GType
sil_filter_background_element_color_get_type (void)
{
  static GType filter_background_element_color = 0;

  if (g_once_init_enter (&filter_background_element_color)) {
    static GEnumValue filter_background_element_color_types[] = {
      {SIL_FILTER_BACKGROUND_ELEMENT_COLOR_WHITE,
          "White", "white"},
      {SIL_FILTER_BACKGROUND_ELEMENT_COLOR_BLACK,
          "Black", "black"},
      {SIL_FILTER_BACKGROUND_ELEMENT_COLOR_RED,
          "Red", "red"},
      {SIL_FILTER_BACKGROUND_ELEMENT_COLOR_GREEN,
          "Green", "green"},
      {SIL_FILTER_BACKGROUND_ELEMENT_COLOR_BLUE,
          "Blue", "blue"},
      {SIL_FILTER_BACKGROUND_ELEMENT_COLOR_YELLOW,
          "Yellow", "yellow"},
      {SIL_FILTER_BACKGROUND_ELEMENT_COLOR_MAGENTA,
          "Magenta", "magenta"},
      {SIL_FILTER_BACKGROUND_ELEMENT_COLOR_CYAN,
          "Cyan", "cyan"},
      {0, NULL, NULL},
    };

    GType temp = g_enum_register_static ("SilFilterBackgroundElementColor",
        filter_background_element_color_types);

    g_once_init_leave (&filter_background_element_color, temp);
  }

  return filter_background_element_color;
}

GST_DEBUG_CATEGORY (sil_filter_background_element_debug);

#define DEBUG_INIT \
    GST_DEBUG_CATEGORY_INIT (sil_filter_background_element_debug, \
    "silfilterbackgroundelement", 0, "silfilterbackgroundelement element");

#define sil_filter_background_element_parent_class parent_class

enum
{
  PROP_0,
  PROP_MODEL_FILE,
  PROP_COLOR,
  PROP_INPUT_IMAGE_FORMAT,
  PROP_INPUT_OFFSET,
  PROP_INPUT_SCALE
};

#define DEFAULT_INPUT_TENSOR_OFFSET 0.0
#define DEFAULT_INPUT_TENSOR_SCALE  255.0

struct _SilFilterBackgroundElement
{
  GstBaseTransform base_transform;

  SilFilterBackgroundElementColor color;

  gchar *model_file;
  sil::onnx::Client *client;
  gboolean onnx_disabled;
  GstVideoInfo video_info;
};

G_DEFINE_TYPE_WITH_CODE (SilFilterBackgroundElement,
    sil_filter_background_element,
    GST_TYPE_BASE_TRANSFORM, DEBUG_INIT);

static GstStaticPadTemplate sil_filter_background_element_src_pad_template =
GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (GST_VIDEO_CAPS_MAKE ("{ RGB }")));

static GstStaticPadTemplate sil_filter_background_element_sink_pad_template =
GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (GST_VIDEO_CAPS_MAKE ("{ RGB }")));

static void
sil_filter_background_element_init (SilFilterBackgroundElement * self)
{
  self->client = new sil::onnx::Client (GST_ELEMENT (self));

  self->color = SIL_FILTER_BACKGROUND_ELEMENT_COLOR_DEFAULT;

  self->onnx_disabled = TRUE;
  self->model_file = nullptr;

  self->client->setInputImageOffset (DEFAULT_INPUT_TENSOR_OFFSET);
  self->client->setInputImageScale (DEFAULT_INPUT_TENSOR_SCALE);
  self->client->setInputImageFormat (sil::onnx::InputImageFormat::HWC);
}

static void
sil_filter_background_element_finalize (GObject * o)
{
  SilFilterBackgroundElement *self = SIL_FILTER_BACKGROUND_ELEMENT (o);

  if (self->client)
    delete self->client;

  g_clear_pointer(&self->model_file, g_free);

  G_OBJECT_CLASS (sil_filter_background_element_parent_class)->finalize (o);
}

static void
sil_filter_background_element_set_property (GObject * o, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  SilFilterBackgroundElement *self = SIL_FILTER_BACKGROUND_ELEMENT (o);

  switch (prop_id) {
    case PROP_MODEL_FILE: {
      const gchar *filename = g_value_get_string (value);
      if (filename && g_file_test (filename,
          static_cast <GFileTest> (
              G_FILE_TEST_EXISTS | G_FILE_TEST_IS_REGULAR))) {
        g_clear_pointer (&self->model_file, g_free);
        self->model_file = g_strdup (filename);
        self->onnx_disabled = FALSE;
      } else {
        GST_WARNING_OBJECT (self, "Could not find model file %s", filename);
      }
      break;
    }

    case PROP_COLOR:
      self->color = static_cast <SilFilterBackgroundElementColor> (
          g_value_get_enum (value));
      break;

    case PROP_INPUT_IMAGE_FORMAT:
      switch (g_value_get_enum (value)) {
        case SIL_FILTER_BACKGROUND_ELEMENT_INPUT_IMAGE_FORMAT_HWC:
          self->client->setInputImageFormat (sil::onnx::InputImageFormat::HWC);
          break;
        case SIL_FILTER_BACKGROUND_ELEMENT_INPUT_IMAGE_FORMAT_CHW:
          self->client->setInputImageFormat (sil::onnx::InputImageFormat::CHW);
          break;
        default:
          g_warn_if_reached ();
          break;
      }
      break;

    case PROP_INPUT_OFFSET:
      self->client->setInputImageOffset (g_value_get_float (value));
      break;

    case PROP_INPUT_SCALE:
      self->client->setInputImageScale (g_value_get_float (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (o, prop_id, pspec);
      break;
  }
}

static void
sil_filter_background_element_get_property (GObject * o, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  SilFilterBackgroundElement *self = SIL_FILTER_BACKGROUND_ELEMENT (o);

  switch (prop_id) {
    case PROP_MODEL_FILE:
      g_value_set_string (value, self->model_file);
      break;

    case PROP_COLOR:
      g_value_set_enum (value, self->color);
      break;

    case PROP_INPUT_IMAGE_FORMAT:
      switch (self->client->getInputImageFormat ()) {
        case sil::onnx::InputImageFormat::HWC:
          g_value_set_enum (value,
              SIL_FILTER_BACKGROUND_ELEMENT_INPUT_IMAGE_FORMAT_HWC);
          break;
        case sil::onnx::InputImageFormat::CHW:
          g_value_set_enum (value,
              SIL_FILTER_BACKGROUND_ELEMENT_INPUT_IMAGE_FORMAT_CHW);
          break;
        default:
          g_warn_if_reached ();
          break;
      }
      break;

    case PROP_INPUT_OFFSET:
      g_value_set_float (value, self->client->getInputImageOffset ());
      break;

    case PROP_INPUT_SCALE:
      g_value_set_float (value, self->client->getInputImageScale ());
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (o, prop_id, pspec);
      break;
  }
}

static gboolean
sil_filter_background_element_create_session (GstBaseTransform * trans)
{
  SilFilterBackgroundElement *self = SIL_FILTER_BACKGROUND_ELEMENT (trans);

  GST_OBJECT_LOCK (self);

  if (self->onnx_disabled) {
    GST_OBJECT_UNLOCK (self);
    return FALSE;
  }

  if (self->client->hasSession ()) {
    GST_OBJECT_UNLOCK (self);
    return TRUE;
  }

  if (self->model_file) {
    if (!self->client->createSession (self->model_file)) {
      GST_ERROR_OBJECT (self,
          "Unable to create ONNX session. Model is disabled.");
      self->onnx_disabled = TRUE;
    }
  } else {
    self->onnx_disabled = TRUE;
    GST_ELEMENT_ERROR (self, STREAM, FAILED, (NULL),
        ("Model file not provided"));
  }

  GST_OBJECT_UNLOCK (self);

  if (self->onnx_disabled)
    gst_base_transform_set_passthrough (GST_BASE_TRANSFORM (self), TRUE);

  return TRUE;
}

static GstCaps *
sil_filter_background_element_transform_caps (GstBaseTransform *
    trans, GstPadDirection direction, GstCaps * caps, GstCaps * filter_caps)
{
  SilFilterBackgroundElement *self = SIL_FILTER_BACKGROUND_ELEMENT (trans);
  GstCaps *other_caps;
  GstCaps *restrictions;

  if (!sil_filter_background_element_create_session (trans))
    return NULL;

  GST_LOG_OBJECT (self, "transforming caps %" GST_PTR_FORMAT, caps);

  if (gst_base_transform_is_passthrough (trans))
    return gst_caps_ref (caps);

  restrictions = gst_caps_new_empty_simple ("video/x-raw");
  if (self->client->isFixedInputImageSize ())
    gst_caps_set_simple (restrictions, "width", G_TYPE_INT,
      self->client->getWidth (), "height", G_TYPE_INT,
      self->client->getHeight (), NULL);

  if (self->client->getInputImageDataType() == sil::onnx::TensorDataType::Uint8
      && self->client->getInputImageScale() == 1.0
      && self->client->getInputImageOffset() == 0.0) {
    switch (self->client->getChannels()) {
      case 1:
        gst_caps_set_simple (restrictions, "format", G_TYPE_STRING, "GRAY8",
          NULL);
        break;

      case 3:
        switch (self->client->getInputImageFormat ()) {
          case sil::onnx::InputImageFormat::HWC:
            gst_caps_set_simple (restrictions, "format", G_TYPE_STRING, "RGB",
                NULL);
            break;
          case sil::onnx::InputImageFormat::CHW:
            gst_caps_set_simple (restrictions, "format", G_TYPE_STRING, "RGBP",
                NULL);
            break;
        }
        break;

      case 4:
        switch (self->client->getInputImageFormat ()) {
          case sil::onnx::InputImageFormat::HWC:
            gst_caps_set_simple (restrictions, "format", G_TYPE_STRING, "RGBA",
                NULL);
          break;
          case sil::onnx::InputImageFormat::CHW:
            gst_caps_set_simple (restrictions, "format", G_TYPE_STRING, "RGBAP",
                NULL);
          break;
        }
        break;

      default:
        GST_ERROR_OBJECT (self, "Invalid number of channels %d",
            self->client->getChannels());
        return NULL;
    }
  }

  GST_DEBUG_OBJECT(self, "Applying caps restrictions: %" GST_PTR_FORMAT,
    restrictions);

  other_caps = gst_caps_intersect_full (caps, restrictions,
      GST_CAPS_INTERSECT_FIRST);
  gst_caps_unref (restrictions);

  if (filter_caps) {
    GstCaps *tmp = gst_caps_intersect_full (
        other_caps, filter_caps, GST_CAPS_INTERSECT_FIRST);
    gst_caps_replace (&other_caps, tmp);
    gst_caps_unref (tmp);
  }

  return other_caps;
}

static gboolean
sil_filter_background_element_set_caps (GstBaseTransform * trans,
    GstCaps * incaps, GstCaps * outcaps)
{
  SilFilterBackgroundElement *self = SIL_FILTER_BACKGROUND_ELEMENT (trans);

  if (!gst_video_info_from_caps (&self->video_info, incaps)) {
    GST_ERROR_OBJECT (self, "Failed to parse caps");
    return FALSE;
  }

  self->client->parseDimensions (self->video_info);
  return TRUE;
}

static void
sil_filter_background_element_segment (const Ort::Value &segmentation_tensor,
    guint8 *frame_data, SilFilterBackgroundElementColor color)
{
  ONNXTensorElementDataType tensorType =
      segmentation_tensor.GetTensorTypeAndShapeInfo ().GetElementType ();
  size_t num_elements =
      segmentation_tensor.GetTensorTypeAndShapeInfo ().GetElementCount ();

  if (tensorType == ONNX_TENSOR_ELEMENT_DATA_TYPE_FLOAT) {
    const float *segmentation_data =
        segmentation_tensor.GetTensorData <float> ();
    guint i = 0, p = 0;

    /* Do the segmentation using the configured color */
    for (i = 0; i < num_elements; i++) {
      if (segmentation_data[i] > 0.5) {
        p += 3;
      } else {
        switch (color) {
          case SIL_FILTER_BACKGROUND_ELEMENT_COLOR_WHITE:
            frame_data [p++] = 255;
            frame_data [p++] = 255;
            frame_data [p++] = 255;
            break;

          case SIL_FILTER_BACKGROUND_ELEMENT_COLOR_BLACK:
            frame_data [p++] = 0;
            frame_data [p++] = 0;
            frame_data [p++] = 0;
            break;

          case SIL_FILTER_BACKGROUND_ELEMENT_COLOR_RED:
            frame_data [p++] = 255;
            frame_data [p++] = 0;
            frame_data [p++] = 0;
            break;

          case SIL_FILTER_BACKGROUND_ELEMENT_COLOR_GREEN:
            frame_data [p++] = 0;
            frame_data [p++] = 255;
            frame_data [p++] = 0;
            break;

          case SIL_FILTER_BACKGROUND_ELEMENT_COLOR_BLUE:
            frame_data [p++] = 0;
            frame_data [p++] = 0;
            frame_data [p++] = 255;
            break;

          case SIL_FILTER_BACKGROUND_ELEMENT_COLOR_YELLOW:
            frame_data [p++] = 255;
            frame_data [p++] = 255;
            frame_data [p++] = 0;
            break;

          case SIL_FILTER_BACKGROUND_ELEMENT_COLOR_MAGENTA:
            frame_data [p++] = 255;
            frame_data [p++] = 0;
            frame_data [p++] = 255;
            break;

          case SIL_FILTER_BACKGROUND_ELEMENT_COLOR_CYAN:
            frame_data [p++] = 0;
            frame_data [p++] = 255;
            frame_data [p++] = 255;
            break;

          default:
            break;
        }
      }
    }
  }
}

static gboolean
sil_filter_background_element_process (GstBaseTransform * trans,
    GstBuffer * buf)
{
  SilFilterBackgroundElement *self = SIL_FILTER_BACKGROUND_ELEMENT (trans);
  GstMapInfo info;

  if (!gst_buffer_map (buf, &info, GST_MAP_READ))
    return FALSE;

  try {
    std::vector<Ort::Value> outputs = self->client->run (info.data,
        self->video_info);
    sil_filter_background_element_segment (outputs[0], info.data, self->color);
  } catch (Ort::Exception &ortex) {
    GST_ERROR_OBJECT (self, "%s", ortex.what ());
    gst_buffer_unmap (buf, &info);
    return FALSE;
  }

  gst_buffer_unmap (buf, &info);
  return TRUE;
}

static GstFlowReturn
sil_filter_background_element_transform_ip (GstBaseTransform * trans,
    GstBuffer * buf)
{
  if (!gst_base_transform_is_passthrough (trans)
      && !sil_filter_background_element_process (trans, buf)) {
    GST_ELEMENT_ERROR (trans, STREAM, FAILED,
        (NULL), ("Failed to filter background from frame"));
    return GST_FLOW_ERROR;
  }

  return GST_FLOW_OK;
}

static void
sil_filter_background_element_class_init (
    SilFilterBackgroundElementClass * klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);
  GstBaseTransformClass *bt_class = GST_BASE_TRANSFORM_CLASS (klass);

  gst_element_class_add_static_pad_template (element_class,
      &sil_filter_background_element_src_pad_template);
  gst_element_class_add_static_pad_template (element_class,
      &sil_filter_background_element_sink_pad_template);
  gst_element_class_set_metadata (element_class,
      "Filter Background", "Filter/Effect/Video",
      "Filters the background of a video", "<julian.bouzas@collabora.com>");

  gobject_class->finalize = sil_filter_background_element_finalize;
  gobject_class->set_property = sil_filter_background_element_set_property;
  gobject_class->get_property = sil_filter_background_element_get_property;

  bt_class->transform_caps = sil_filter_background_element_transform_caps;
  bt_class->set_caps = sil_filter_background_element_set_caps;
  bt_class->transform_ip = sil_filter_background_element_transform_ip;

  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_MODEL_FILE,
      g_param_spec_string ("model-file",
          "ONNX model file", "ONNX model file", NULL, (GParamFlags)
          (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));

  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_COLOR,
      g_param_spec_enum ("color",
          "Color",
          "Color",
          SIL_TYPE_FILTER_BACKGROUND_ELEMENT_COLOR,
          SIL_FILTER_BACKGROUND_ELEMENT_COLOR_DEFAULT,
          static_cast <GParamFlags> (
              G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));

  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_INPUT_IMAGE_FORMAT,
      g_param_spec_enum ("input-image-format",
          "Input image format",
          "Input image format",
          SIL_TYPE_FILTER_BACKGROUND_ELEMENT_INPUT_IMAGE_FORMAT,
          SIL_FILTER_BACKGROUND_ELEMENT_INPUT_IMAGE_FORMAT_HWC,
          static_cast <GParamFlags> (
              G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));

  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_INPUT_OFFSET,
      g_param_spec_float ("input-tensor-offset",
          "Input tensor offset",
          "offset each tensor value by this value",
          -G_MAXFLOAT, G_MAXFLOAT, DEFAULT_INPUT_TENSOR_OFFSET,
          static_cast <GParamFlags> (
              G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));

  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_INPUT_SCALE,
      g_param_spec_float ("input-tensor-scale",
          "Input tensor scale",
          "Divide each tensor value by this value",
          G_MINFLOAT, G_MAXFLOAT, DEFAULT_INPUT_TENSOR_SCALE,
          static_cast <GParamFlags> (
              G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));
}

GstElement *
sil_filter_background_element_new (const gchar * name)
{
  return GST_ELEMENT (g_object_new (SIL_TYPE_FILTER_BACKGROUND_ELEMENT,
      "name", name,
      NULL));
}
