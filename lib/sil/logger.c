/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include <stdio.h>
#include <time.h>

#include "logger.h"
#include "private/logger.h"

#define SIL_LOGGER_MAX_MESSAGE_SIZE 1024

extern SilLogger * sil_logger_;

enum {
  LOGGER_PROP_0,
  LOGGER_PROP_LEVEL,
};

struct _SilLogger
{
  GObject parent;

  SilLoggerLevel level;
};

G_DEFINE_TYPE (SilLogger, sil_logger, G_TYPE_OBJECT)

static void
sil_logger_init (SilLogger * self)
{
}

static void
sil_logger_constructed (GObject *object)
{
  SilLogger *self = SIL_LOGGER (object);
  char * val_str = NULL;
  SilLoggerLevel val = SIL_LOGGER_LEVEL_NONE;

  /* Check if new level is set from env */
  val_str = getenv ("SILHOUETTE_DEBUG");
  if (val_str && sscanf (val_str, "%u", &val) == 1 &&
      val >= SIL_LOGGER_LEVEL_NONE)
    self->level = val;

  G_OBJECT_CLASS (sil_logger_parent_class)->constructed (object);
}

static void
sil_logger_set_property (GObject * object, guint property_id,
    const GValue * value, GParamSpec * pspec)
{
  SilLogger *self = SIL_LOGGER (object);

  switch (property_id) {
  case LOGGER_PROP_LEVEL:
    self->level = g_value_get_enum (value);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
sil_logger_get_property (GObject * object, guint property_id, GValue * value,
    GParamSpec * pspec)
{
  SilLogger *self = SIL_LOGGER (object);

  switch (property_id) {
  case LOGGER_PROP_LEVEL:
    g_value_set_enum (value, self->level);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
sil_logger_class_init (SilLoggerClass * klass)
{
  GObjectClass * object_class = (GObjectClass *) klass;

  object_class->constructed = sil_logger_constructed;
  object_class->get_property = sil_logger_get_property;
  object_class->set_property = sil_logger_set_property;

  g_object_class_install_property (object_class, LOGGER_PROP_LEVEL,
      g_param_spec_enum ("level", "level", "The logging level",
          SIL_TYPE_LOGGER_LEVEL, SIL_LOGGER_LEVEL_WARN,
          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));
}

static G_GNUC_PRINTF (4, 0) void
sil_logger_logv (SilLogger *self, GObject *object, SilLoggerLevel level,
    const gchar *fmt, va_list args)
{
  g_return_if_fail (SIL_IS_LOGGER (self));

  if (self->level >= level) {
    struct timespec time;
    gchar msg[SIL_LOGGER_MAX_MESSAGE_SIZE];
    static const gchar *logger_level_text_[] = {
      [SIL_LOGGER_LEVEL_ERROR] = "E",
      [SIL_LOGGER_LEVEL_WARN] = "W",
      [SIL_LOGGER_LEVEL_INFO] = "I",
      [SIL_LOGGER_LEVEL_DEBUG] = "D",
      [SIL_LOGGER_LEVEL_TRACE] = "T",
    };

    clock_gettime (CLOCK_REALTIME, &time);
    g_vsnprintf (msg, SIL_LOGGER_MAX_MESSAGE_SIZE, fmt, args);
    if (object) {
      g_return_if_fail (G_IS_OBJECT (object));
      g_printerr ("[%s] [%lu.%lu] [%s|%p] %s\n", logger_level_text_ [level],
          time.tv_sec, time.tv_nsec, G_OBJECT_TYPE_NAME (object), object, msg);
    } else {
      g_printerr ("[%s] [%lu.%lu] %s\n", logger_level_text_ [level],
          time.tv_sec, time.tv_nsec, msg);
    }
  }
}

G_GNUC_PRINTF (3, 0) void
sil_logv (GObject *object, SilLoggerLevel level, const gchar *fmt, va_list args)
{
  g_return_if_fail (sil_logger_);

  sil_logger_logv (sil_logger_, object, level, fmt, args);
}

G_GNUC_PRINTF (3, 0) void
sil_log (GObject *object, SilLoggerLevel level, const gchar *fmt, ...)
{
  va_list args;
  va_start (args, fmt);
  sil_logv (object, level, fmt, args);
  va_end (args);
}

SilLogger *
sil_logger_new (SilLoggerLevel level)
{
  return g_object_new (SIL_TYPE_LOGGER, "level", level, NULL);
}
