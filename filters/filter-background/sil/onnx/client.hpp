/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#pragma once

#include <onnxruntime_cxx_api.h>

#include <gst/video/video.h>

namespace sil {
namespace onnx {

/* The Tensor Data Type */
enum class TensorDataType { Uint8, Float };

/* The Input Image Format enum */
enum class InputImageFormat { HWC, CHW };

/* The Client class */
class Client
{
 public:
  /* Constructor */
  Client (GstElement *element);

  /* Destructor */
  virtual ~Client (void);

 public:
  /* Methods */
  bool createSession (std::string modelFile);
  bool hasSession (void);
  void setInputImageFormat (InputImageFormat format);
  InputImageFormat getInputImageFormat (void);
  TensorDataType getInputImageDataType (void);
  void setInputImageOffset (float offset);
  float getInputImageOffset ();
  void setInputImageScale (float offset);
  float getInputImageScale ();
  std::vector <Ort::Value> run (uint8_t * img_data, GstVideoInfo vinfo);
  std::vector <const char *> genOutputNamesRaw (void);
  bool isFixedInputImageSize (void);
  int32_t getWidth (void);
  int32_t getHeight (void);
  int32_t getChannels (void);
  void parseDimensions (GstVideoInfo vinfo);

 private:
  /* Copy Constructor */
  Client (const Client&) = delete;

  /* Move Constructor */
  Client (Client &&) = delete;

  /* Copy-Assign Constructor */
  Client& operator=(const Client&) = delete;

  /* Move-Assign Constructr */
  Client& operator=(Client &&) = delete;

 private:
  /* Methods */
  void setInputImageDataType (TensorDataType data_type);
  template < typename T>
  void convert_image_remove_alpha (T *dest, InputImageFormat input_image_format,
      uint8_t **srcPtr, uint32_t srcSamplesPerPixel, uint32_t stride, T offset, T div);
  bool doRun (uint8_t * img_data, GstVideoInfo vinfo, std::vector < Ort::Value > &model_output);

 private:
  /* Members */
  GstElement *element_;
  Ort::Env env_;
  Ort::Session *session_;
  int32_t width_;
  int32_t height_;
  int32_t channels_;
  uint8_t *dest_;
  std::vector <const char *> output_names_raw_;
  std::vector <Ort::AllocatedStringPtr> output_names_;
  InputImageFormat input_image_format_;
  TensorDataType input_data_type_;
  size_t input_data_type_size_;
  bool is_fixed_input_image_size_;
  float input_tensor_offset_;
  float input_tensor_scale_;
};

}  /* onnx */
}  /* sil */
