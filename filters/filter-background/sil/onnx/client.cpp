/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include <sstream>

#include "client.hpp"

namespace sil {
namespace onnx {

template < typename T >
std::ostream & operator<< (std::ostream & os, const std::vector < T > &v)
{
  os << "[";
  for (size_t i = 0; i < v.size (); ++i) {
    os << v[i];
    if (i != v.size () - 1)
      os << ", ";
  }
  os << "]";

  return os;
}

Client::Client (GstElement *element):
    element_ (element),
    session_ (nullptr),
    width_ (0),
    height_ (0),
    channels_ (0),
    dest_ (nullptr),
    input_image_format_ (InputImageFormat::HWC),
    input_data_type_ (TensorDataType::Uint8),
    input_data_type_size_ (sizeof (uint8_t)),
    is_fixed_input_image_size_ (false),
    input_tensor_offset_ (0.0),
    input_tensor_scale_ (255.0)
{
}

Client::~Client () {
  delete session_;
  delete[] dest_;
}

int32_t
Client::getWidth (void)
{
  return width_;
}

int32_t
Client::getHeight (void)
{
  return height_;
}

int32_t
Client::getChannels (void)
{
  return channels_;
}

bool
Client::isFixedInputImageSize (void)
{
  return is_fixed_input_image_size_;
}

void
Client::setInputImageFormat (InputImageFormat format)
{
  input_image_format_ = format;
}

InputImageFormat
Client::getInputImageFormat (void)
{
  return input_image_format_;
}

void
Client::setInputImageDataType(TensorDataType data_type)
{
  input_data_type_ = data_type;

  switch (input_data_type_) {
    case TensorDataType::Uint8:
      input_data_type_size_ = sizeof (uint8_t);
      break;
    case TensorDataType::Float:
      input_data_type_size_ = sizeof (float);
      break;
    default:
      g_warn_if_reached ();
	  break;
  };
}

void
Client::setInputImageOffset (float offset)
{
  input_tensor_offset_ = offset;
}

float
Client::getInputImageOffset ()
{
  return input_tensor_offset_;
}

void
Client::setInputImageScale (float scale)
{
  input_tensor_scale_ = scale;
}

float
Client::getInputImageScale ()
{
  return input_tensor_scale_;
}

TensorDataType
Client::getInputImageDataType(void)
{
  return input_data_type_;
}

std::vector <const char *>
Client::genOutputNamesRaw (void)
{
  if (!output_names_.empty () && output_names_raw_.size () != output_names_.size ()) {
    output_names_raw_.resize (output_names_.size ());
    for (size_t i = 0; i < output_names_raw_.size (); i++)
      output_names_raw_[i] = output_names_[i].get ();
  }

  return output_names_raw_;
}

bool
Client::hasSession (void)
{
  return session_ != nullptr;
}

bool
Client::createSession (std::string modelFile)
{
  if (session_)
    return true;

  try {
    Ort::SessionOptions session_options;
    Ort::AllocatorWithDefaultOptions allocator;

    session_options.SetGraphOptimizationLevel (
        GraphOptimizationLevel::ORT_ENABLE_EXTENDED);

    env_ = Ort::Env (OrtLoggingLevel::ORT_LOGGING_LEVEL_WARNING,  "onnx");
    session_ = new Ort::Session (env_, modelFile.c_str (), session_options);

    auto inputTypeInfo = session_->GetInputTypeInfo (0);

    std::vector < int64_t > inputDims =
        inputTypeInfo.GetTensorTypeAndShapeInfo ().GetShape ();

    if (input_image_format_ == InputImageFormat::HWC) {
      height_ = inputDims[1];
      width_ = inputDims[2];
      channels_ = inputDims[3];
    } else {
      channels_ = inputDims[1];
      height_ = inputDims[2];
      width_ = inputDims[3];
    }

    GST_INFO_OBJECT (element_, "Model input dimensions: h=%d w=%d c=%d",
        height_, width_, channels_);
    is_fixed_input_image_size_ = width_ > 0 && height_ > 0;
    GST_DEBUG_OBJECT (element_, "Number of Output Nodes: %d",
        (gint) session_->GetOutputCount ());

    ONNXTensorElementDataType elementType =
        inputTypeInfo.GetTensorTypeAndShapeInfo ().GetElementType ();

    switch (elementType) {
      case ONNX_TENSOR_ELEMENT_DATA_TYPE_UINT8:
        setInputImageDataType (TensorDataType::Uint8);
        break;
      case ONNX_TENSOR_ELEMENT_DATA_TYPE_FLOAT:
        setInputImageDataType (TensorDataType::Float);
        break;
      default:
        GST_ERROR_OBJECT (element_,
            "Only input tensors of type uint8 and float are supported");
        return false;
    }

    auto input_name = session_->GetInputNameAllocated (0, allocator);
    GST_INFO_OBJECT (element_, "Input name: %s", input_name.get ());

    for (size_t i = 0; i < session_->GetOutputCount (); ++i) {
      auto output_name = session_->GetOutputNameAllocated (i, allocator);
      GST_INFO_OBJECT (element_, "Output name %lu:%s", i, output_name.get ());
      output_names_.push_back (std::move (output_name));
    }

    genOutputNamesRaw ();
  } catch (Ort::Exception & ortex) {
    GST_ERROR_OBJECT (element_, "%s", ortex.what ());
    return false;
  }

  return true;
}

void
Client::parseDimensions (GstVideoInfo vinfo)
{
  int32_t newWidth = is_fixed_input_image_size_ ? width_ : vinfo.width;
  int32_t newHeight = is_fixed_input_image_size_ ? height_ : vinfo.height;

  if (!is_fixed_input_image_size_)
    GST_WARNING_OBJECT (element_, "Allocating before knowing model input size");

  if (!dest_ || width_ * height_ < newWidth * newHeight) {
    delete[] dest_;
    dest_ = new uint8_t[newWidth * newHeight * channels_ * input_data_type_size_];
  }

  width_ = newWidth;
  height_ = newHeight;
}

std::vector <Ort::Value>
Client::run (uint8_t * img_data, GstVideoInfo vinfo)
{
  std::vector < Ort::Value > model_output;

  doRun (img_data, vinfo, model_output);

  return model_output;
}

bool
Client::doRun (uint8_t * img_data, GstVideoInfo vinfo,
    std::vector <Ort::Value> &model_output)
{
  if (!img_data)
    return false;

  Ort::AllocatorWithDefaultOptions allocator;
  auto inputName = session_->GetInputNameAllocated (0, allocator);
  auto inputTypeInfo = session_->GetInputTypeInfo (0);
  std::vector <int64_t> inputDims =
      inputTypeInfo.GetTensorTypeAndShapeInfo ().GetShape ();
  inputDims[0] = 1;
  if (input_image_format_ == InputImageFormat::HWC) {
    inputDims[1] = height_;
    inputDims[2] = width_;
  } else {
    inputDims[2] = height_;
    inputDims[3] = width_;
  }

  std::ostringstream buffer;
  buffer << inputDims;

  /* Copy video frame */
  uint8_t *srcPtr[3] = { img_data, img_data + 1, img_data + 2 };
  uint32_t srcSamplesPerPixel = 3;
  switch (vinfo.finfo->format) {
    case GST_VIDEO_FORMAT_RGBA:
      srcSamplesPerPixel = 4;
      break;
    case GST_VIDEO_FORMAT_BGRA:
      srcSamplesPerPixel = 4;
      srcPtr[0] = img_data + 2;
      srcPtr[1] = img_data + 1;
      srcPtr[2] = img_data + 0;
      break;
    case GST_VIDEO_FORMAT_ARGB:
      srcSamplesPerPixel = 4;
      srcPtr[0] = img_data + 1;
      srcPtr[1] = img_data + 2;
      srcPtr[2] = img_data + 3;
      break;
    case GST_VIDEO_FORMAT_ABGR:
      srcSamplesPerPixel = 4;
      srcPtr[0] = img_data + 3;
      srcPtr[1] = img_data + 2;
      srcPtr[2] = img_data + 1;
      break;
    case GST_VIDEO_FORMAT_BGR:
      srcPtr[0] = img_data + 2;
      srcPtr[1] = img_data + 1;
      srcPtr[2] = img_data + 0;
      break;
    default:
      break;
  }

  uint32_t stride = vinfo.stride[0];
  const size_t input_tensor_size = width_ * height_ * channels_ * input_data_type_size_;
  auto memoryInfo = Ort::MemoryInfo::CreateCpu (
      OrtAllocatorType::OrtArenaAllocator, OrtMemType::OrtMemTypeDefault);

  std::vector<Ort::Value> input_tensors;

  switch (input_data_type_) {
    case TensorDataType::Uint8: {
      uint8_t *src_data;

      if (input_tensor_offset_ == 0.0 && input_tensor_scale_ == 1.0) {
        src_data = img_data;
      } else {
        convert_image_remove_alpha (dest_, input_image_format_, srcPtr,
            srcSamplesPerPixel, stride, (uint8_t)input_tensor_offset_,
            (uint8_t)input_tensor_scale_);
        src_data = dest_;
      }

      input_tensors.push_back (Ort::Value::CreateTensor < uint8_t > (
            memoryInfo, src_data, input_tensor_size, inputDims.data (),
            inputDims.size ()));
      break;
    }

    case TensorDataType::Float: {
      convert_image_remove_alpha ((float*)dest_, input_image_format_, srcPtr,
          srcSamplesPerPixel, stride, (float)input_tensor_offset_, (float)
          input_tensor_scale_);
      input_tensors.push_back (Ort::Value::CreateTensor < float > (
            memoryInfo, (float*)dest_, input_tensor_size, inputDims.data (),
            inputDims.size ()));
      }
      break;

    default:
      g_warn_if_reached ();
      break;
  }

  std::vector < const char *>inputNames { inputName.get () };
  model_output = session_->Run (Ort::RunOptions {nullptr},
      inputNames.data (),
      input_tensors.data (), 1, output_names_raw_.data (),
      output_names_raw_.size ());

  return true;
}

template <typename T>
void
Client::convert_image_remove_alpha (T *dst, InputImageFormat input_image_format,
    uint8_t **srcPtr, uint32_t srcSamplesPerPixel, uint32_t stride, T offset,
    T div)
{
  size_t destIndex = 0;
  T tmp;

  if (input_image_format == InputImageFormat::HWC) {
    for (int32_t j = 0; j < height_; ++j) {
      for (int32_t i = 0; i < width_; ++i) {
        for (int32_t k = 0; k < channels_; ++k) {
          tmp = *srcPtr[k];
          tmp += offset;
          dst[destIndex++] = (T)(tmp / div);
          srcPtr[k] += srcSamplesPerPixel;
        }
      }

      /* Correct for stride */
      for (uint32_t k = 0; k < 3; ++k)
        srcPtr[k] += stride - srcSamplesPerPixel * width_;
    }
  } else {
    size_t frameSize = width_ * height_;
    T *destPtr[3] = { dst, dst + frameSize, dst + 2 * frameSize };

    for (int32_t j = 0; j < height_; ++j) {
      for (int32_t i = 0; i < width_; ++i) {
        for (int32_t k = 0; k < channels_; ++k) {
          tmp = *srcPtr[k];
          tmp += offset;
          destPtr[k][destIndex] = (T)(tmp / div);
          srcPtr[k] += srcSamplesPerPixel;
        }
        destIndex++;
      }

      /* Correct for stride */
      for (uint32_t k = 0; k < 3; ++k)
        srcPtr[k] += stride - srcSamplesPerPixel * width_;
    }
  }
}

}  /* onnx */
}  /* sil */
