/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include <gst/app/gstappsink.h>
#include <gst/video/video-format.h>

#include "logger.h"
#include "input.h"
#include "filter.h"
#include "source.h"
#include "channel.h"

enum {
  CHANNEL_PROP_0,
  CHANNEL_PROP_REMOTE_NAME,
  CHANNEL_PROP_FILTER_FACTORY,
  CHANNEL_PROP_INPUT_PATH,
  CHANNEL_PROP_WIDTH,
  CHANNEL_PROP_HEIGHT,
  CHANNEL_PROP_FPS_N,
  CHANNEL_PROP_FPS_D,
};

struct _SilChannel
{
  SilPipeline parent;

  /* Props */
  gchar *input_name;
  gchar *source_name;
  gchar *remote_name;
  gchar *filter_factory;
  gchar *input_path;
  guint width;
  guint height;
  guint fps_n;
  guint fps_d;

  SilInput *input;
  SilFilter *filter;
  SilSource *source;

  GstElement *appsrc;
  GstElement *filter_element;
  GstElement *appsink;
};

G_DEFINE_TYPE (SilChannel, sil_channel, SIL_TYPE_PIPELINE)

static void
sil_channel_init (SilChannel * self)
{
  self->width = SIL_SOURCE_DEFAULT_WIDTH;
  self->height = SIL_SOURCE_DEFAULT_HEIGHT;
  self->fps_n = SIL_SOURCE_DEFAULT_FPS_N;
  self->fps_d = SIL_SOURCE_DEFAULT_FPS_D;
}

static GstFlowReturn
on_new_sample (GstElement * appsink, SilChannel * self)
{
  GstSample *sample;
  GstBuffer *buffer;

  sample = gst_app_sink_pull_sample (GST_APP_SINK (appsink));
  buffer = gst_sample_get_buffer (sample);

  if (buffer) {
    sil_log_trace_object (self,
        "Pushing buffer %lu to stream source from channel %s", buffer->pts,
        sil_pipeline_get_name (SIL_PIPELINE (self)));
    sil_source_queue_buffer (self->source, gst_buffer_ref (buffer));
  }

  gst_sample_unref (sample);

  return GST_FLOW_OK;
}

static gboolean
sil_channel_build (SilPipeline * base, GstElement *gst_pipeline)
{
  SilChannel *self = SIL_CHANNEL (base);

  if (!self->filter) {
    sil_log_error_object (self,
        "Channel '%s' could not create filter '%s'", self->filter_factory,
        sil_pipeline_get_name (base));
    goto error;
  }

  /* Create elements */
  self->appsrc = gst_element_factory_make ("appsrc", NULL);
  if (!self->appsrc) {
    sil_log_error_object (self,
        "Channel '%s' could not create appsrc element",
        sil_pipeline_get_name (base));
    goto error;
  }

  self->filter_element = sil_filter_get_element (self->filter);
  if (!self->filter_element) {
    sil_log_error_object (self,
        "Channel '%s' could not get filter element",
        sil_pipeline_get_name (base));
    goto error;
  }

  self->appsink = gst_element_factory_make ("appsink", NULL);
  if (!self->appsink) {
    sil_log_error_object (self,
        "Channel '%s' could not create appsink element",
        sil_pipeline_get_name (base));
    goto error;
  }

  /* Configure appsrc */
  g_object_set (self->appsrc, "format", GST_FORMAT_TIME, NULL);
  g_object_set (self->appsrc, "do-timestamp", TRUE, NULL);

  /* Make appsrc receive buffers from input */
  sil_input_set_appsrc (self->input, self->appsrc);

  /* Configure appsink */
  g_object_set (self->appsink, "sync", FALSE, NULL);
  g_object_set (self->appsink, "emit-signals", TRUE, NULL);
  {
    GstCaps *caps = gst_caps_new_simple ("video/x-raw",
        "format", G_TYPE_STRING, "RGBA",
        "width", G_TYPE_INT, self->width,
        "height", G_TYPE_INT, self->height,
        NULL);
    g_object_set (G_OBJECT (self->appsink), "caps", caps, NULL);
    gst_caps_unref (caps);
  }

  /* Handle appsink's new-sample signal */
  g_signal_connect_object (self->appsink, "new-sample",
      G_CALLBACK (on_new_sample), self, 0);

  /* Add elements */
  gst_bin_add_many (GST_BIN (gst_pipeline),
      gst_object_ref (self->appsrc),
      gst_object_ref (self->filter_element),
      gst_object_ref (self->appsink),
      NULL);

  /* Link elements */
  gst_element_link_many (self->appsrc, self->filter_element, self->appsink,
      NULL);

  return TRUE;

error:
  gst_clear_object (&self->appsrc);
  gst_clear_object (&self->filter_element);
  gst_clear_object (&self->appsink);
  return FALSE;
}

static gboolean
sil_channel_play (SilPipeline * base)
{
  SilChannel *self = SIL_CHANNEL (base);

  /* Connect the input stream */
  if (!sil_pipewire_context_connect (SIL_PIPEWIRE_CONTEXT (self->input))) {
    sil_log_error_object (self,
        "Could not connect channel '%s' input to PipeWire",
        sil_pipeline_get_name (base));
    return FALSE;
  }

  /* Connect the source stream */
  if (!sil_pipewire_context_connect (SIL_PIPEWIRE_CONTEXT (self->source))) {
    sil_log_error_object (self,
        "Could not connect channel '%s' source to PipeWire",
        sil_pipeline_get_name (base));
    return FALSE;
  }

  return TRUE;
}

static void
sil_channel_stop (SilPipeline * base)
{
  SilChannel *self = SIL_CHANNEL (base);

  /* Disconnect the source */
  sil_pipewire_context_disconnect (SIL_PIPEWIRE_CONTEXT (self->source));

  /* Disconnect the input */
  sil_pipewire_context_disconnect (SIL_PIPEWIRE_CONTEXT (self->input));
}

static void
sil_channel_constructed (GObject *object)
{
  SilChannel *self = SIL_CHANNEL (object);
  const gchar *pipeline_name;

  /* Make sure the name is set */
  G_OBJECT_CLASS (sil_channel_parent_class)->constructed (object);

  if (self->filter_factory) {
    self->filter = sil_filter_factory_make (self->filter_factory, NULL);
  } else {
    sil_log_error_object (self,
        "Channel '%s' does not have a valid filter factory",
        sil_pipeline_get_name (SIL_PIPELINE (self)));
  }

  /* Set input name and source name */
  pipeline_name = sil_pipeline_get_name (SIL_PIPELINE (self));
  self->input_name = g_strdup_printf ("%s.input", pipeline_name);
  self->source_name = g_strdup_printf ("%s.source", pipeline_name);

  /* Create the input */
  self->input = sil_input_new (self->remote_name, self->input_name,
      pipeline_name, self->input_path);

  /* Create the source */
  self->source = sil_source_new (self->remote_name, self->source_name,
      pipeline_name);
  g_object_set (G_OBJECT (self->source), "width", self->width, NULL);
  g_object_set (G_OBJECT (self->source), "height", self->height, NULL);
  g_object_set (G_OBJECT (self->source), "fps-n", self->fps_n, NULL);
  g_object_set (G_OBJECT (self->source), "fps-d", self->fps_d, NULL);
  g_object_set (G_OBJECT (self->source), "format", GST_VIDEO_FORMAT_RGBA,
      NULL);
}

static void
sil_channel_set_property (GObject * object, guint property_id,
    const GValue * value, GParamSpec * pspec)
{
  SilChannel *self = SIL_CHANNEL (object);

  switch (property_id) {
    case CHANNEL_PROP_REMOTE_NAME:
      g_clear_pointer (&self->remote_name, g_free);
      self->remote_name = g_value_dup_string (value);
      break;

    case CHANNEL_PROP_FILTER_FACTORY:
      g_clear_pointer (&self->filter_factory, g_free);
      self->filter_factory = g_value_dup_string (value);
      break;

    case CHANNEL_PROP_INPUT_PATH:
      g_clear_pointer (&self->input_path, g_free);
      self->input_path = g_value_dup_string (value);
      break;

    case CHANNEL_PROP_WIDTH:
      self->width = g_value_get_uint (value);
      if (self->source)
        g_object_set (G_OBJECT (self->source), "width", self->width, NULL);
      break;

    case CHANNEL_PROP_HEIGHT:
      self->height = g_value_get_uint (value);
      if (self->source)
        g_object_set (G_OBJECT (self->source), "height", self->height, NULL);
      break;

    case CHANNEL_PROP_FPS_N:
      self->fps_n = g_value_get_uint (value);
      if (self->source)
        g_object_set (G_OBJECT (self->source), "fps-n", self->fps_n, NULL);
      break;

    case CHANNEL_PROP_FPS_D:
      self->fps_d = g_value_get_uint (value);
      if (self->source)
        g_object_set (G_OBJECT (self->source), "fps-d", self->fps_d, NULL);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

static void
sil_channel_get_property (GObject * object, guint property_id,
    GValue * value, GParamSpec * pspec)
{
  SilChannel *self = SIL_CHANNEL (object);

  switch (property_id) {
    case CHANNEL_PROP_REMOTE_NAME:
      g_value_set_string (value, self->remote_name);
      break;

    case CHANNEL_PROP_FILTER_FACTORY:
      g_value_set_string (value, self->filter_factory);
      break;

    case CHANNEL_PROP_INPUT_PATH:
      g_value_set_string (value, self->input_path);
      break;

    case CHANNEL_PROP_WIDTH:
      g_value_set_uint (value, self->width);
      break;

    case CHANNEL_PROP_HEIGHT:
      g_value_set_uint (value, self->height);
      break;

    case CHANNEL_PROP_FPS_N:
      g_value_set_uint (value, self->fps_n);
      break;

    case CHANNEL_PROP_FPS_D:
      g_value_set_uint (value, self->fps_d);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

static void
sil_channel_finalize (GObject * object)
{
  SilChannel *self = SIL_CHANNEL (object);

  gst_clear_object (&self->appsrc);
  gst_clear_object (&self->filter_element);
  gst_clear_object (&self->appsink);

  g_clear_object (&self->input);
  g_clear_object (&self->filter);
  g_clear_object (&self->source);
  g_clear_pointer (&self->input_path, g_free);
  g_clear_pointer (&self->filter_factory, g_free);
  g_clear_pointer (&self->remote_name, g_free);
  g_clear_pointer (&self->source_name, g_free);
  g_clear_pointer (&self->input_name, g_free);

  G_OBJECT_CLASS (sil_channel_parent_class)->finalize (object);
}

static void
sil_channel_class_init (SilChannelClass * klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;
  SilPipelineClass *pipeline_class = (SilPipelineClass *) klass;

  object_class->constructed = sil_channel_constructed;
  object_class->finalize = sil_channel_finalize;
  object_class->set_property = sil_channel_set_property;
  object_class->get_property = sil_channel_get_property;

  pipeline_class->build = sil_channel_build;
  pipeline_class->play = sil_channel_play;
  pipeline_class->stop = sil_channel_stop;

  g_object_class_install_property (object_class, CHANNEL_PROP_REMOTE_NAME,
      g_param_spec_string ("remote-name", "remote-name",
          "The name for the remote to connect to", NULL,
          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (object_class, CHANNEL_PROP_FILTER_FACTORY,
      g_param_spec_string ("filter-factory", "filter-factory",
          "The channel filter factory name", NULL,
          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (object_class, CHANNEL_PROP_INPUT_PATH,
      g_param_spec_string ("input-path", "input-path",
          "The channel input path", NULL,
          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (object_class, CHANNEL_PROP_WIDTH,
      g_param_spec_uint ("width", "width",
          "The width of the channel",
          1, G_MAXUINT, SIL_SOURCE_DEFAULT_WIDTH,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (object_class, CHANNEL_PROP_HEIGHT,
      g_param_spec_uint ("height", "height",
          "The height of the channel",
          1, G_MAXUINT, SIL_SOURCE_DEFAULT_HEIGHT,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (object_class, CHANNEL_PROP_FPS_N,
      g_param_spec_uint ("fps-n", "fps-n",
          "The FPS numerator of the channel",
          0, G_MAXUINT, SIL_SOURCE_DEFAULT_FPS_N,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (object_class, CHANNEL_PROP_FPS_D,
      g_param_spec_uint ("fps-d", "fps-d",
          "The FPS denominator of the channel",
          1, G_MAXUINT, SIL_SOURCE_DEFAULT_FPS_D,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
}

SilChannel *
sil_channel_new (GMainContext *ctx, const gchar *remote_name,
    const gchar * name, const gchar *filter_factory, const gchar *input_path)
{
  return g_object_new (SIL_TYPE_CHANNEL,
      "g-main-context", ctx,
      "remote-name", remote_name,
      "name", name,
      "filter-factory", filter_factory,
      "input-path", input_path,
      NULL);
}

SilFilter *
sil_channel_get_filter (SilChannel *self)
{
  g_return_val_if_fail (SIL_IS_CHANNEL (self), NULL);

  return self->filter ? g_object_ref (self->filter) : NULL;
}

const gchar *
sil_channel_get_input_name (SilChannel * self)
{
  g_return_val_if_fail (SIL_IS_CHANNEL (self), NULL);

  return self->input_name;
}

const gchar *
sil_channel_get_source_name (SilChannel * self)
{
  g_return_val_if_fail (SIL_IS_CHANNEL (self), NULL);

  return self->source_name;
}
