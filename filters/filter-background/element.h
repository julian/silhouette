/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef __SILHOUETTE_FILTER_BACKGROUND_ELEMENT_H__
#define __SILHOUETTE_FILTER_BACKGROUND_ELEMENT_H__

#include <gst/video/gstvideofilter.h>

G_BEGIN_DECLS

/* SilFilterBackgroundElementColor */

typedef enum {
  SIL_FILTER_BACKGROUND_ELEMENT_COLOR_WHITE,
  SIL_FILTER_BACKGROUND_ELEMENT_COLOR_BLACK,
  SIL_FILTER_BACKGROUND_ELEMENT_COLOR_RED,
  SIL_FILTER_BACKGROUND_ELEMENT_COLOR_GREEN,
  SIL_FILTER_BACKGROUND_ELEMENT_COLOR_BLUE,
  SIL_FILTER_BACKGROUND_ELEMENT_COLOR_YELLOW,
  SIL_FILTER_BACKGROUND_ELEMENT_COLOR_MAGENTA,
  SIL_FILTER_BACKGROUND_ELEMENT_COLOR_CYAN,
} SilFilterBackgroundElementColor;

GType sil_filter_background_element_color_get_type (void);
#define SIL_TYPE_FILTER_BACKGROUND_ELEMENT_COLOR (\
    sil_filter_background_element_color_get_type ())

#define SIL_FILTER_BACKGROUND_ELEMENT_COLOR_DEFAULT \
    SIL_FILTER_BACKGROUND_ELEMENT_COLOR_WHITE

/* SilFilterBackgroundElement */

#define SIL_TYPE_FILTER_BACKGROUND_ELEMENT (\
    sil_filter_background_element_get_type ())

G_DECLARE_FINAL_TYPE (SilFilterBackgroundElement,
    sil_filter_background_element, SIL, FILTER_BACKGROUND_ELEMENT,
    GstBaseTransform)

GstElement *
sil_filter_background_element_new (const gchar * name);

G_END_DECLS

#endif
