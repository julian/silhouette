/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef __SILHOUETTE_FILTER_H__
#define __SILHOUETTE_FILTER_H__

#include <gst/gst.h>

#include "macros.h"

G_BEGIN_DECLS

/* SilFilter */

#define SIL_TYPE_FILTER (sil_filter_get_type ())
SIL_API
G_DECLARE_DERIVABLE_TYPE (SilFilter, sil_filter, SIL, FILTER, GObject)

struct _SilFilterClass
{
  GObjectClass parent_class;

  const gchar *description;
  GstElement * (*create_element) (SilFilter * self);
};

SIL_API
GstElement * sil_filter_get_element (SilFilter * self);


/* SilFilterFactory */

#define SIL_TYPE_FILTER_FACTORY (sil_filter_factory_get_type ())
SIL_API
G_DECLARE_FINAL_TYPE (SilFilterFactory, sil_filter_factory, SIL, FILTER_FACTORY,
    GObject)

SIL_API
SilFilterFactory * sil_filter_factory_new (const gchar * factory_name,
    GType type);

SIL_API
const gchar * sil_filter_factory_get_description (SilFilterFactory *self);

SIL_API
SilFilter * sil_filter_factory_construct (SilFilterFactory *self,
    const gchar *name);

SIL_API
SilFilter * sil_filter_factory_make (const gchar * factory_name,
    const gchar * name);


/* SilFilterRegistry */

#define SIL_TYPE_FILTER_REGISTRY (sil_filter_registry_get_type ())
SIL_API
G_DECLARE_FINAL_TYPE (SilFilterRegistry, sil_filter_registry, SIL,
    FILTER_REGISTRY, GObject)

SIL_API
SilFilterFactory * sil_filter_registry_get_factory (SilFilterRegistry *self,
    const gchar *factory_name);

SIL_API
GList * sil_filter_registry_get_factories (SilFilterRegistry *self);

G_END_DECLS

#endif

