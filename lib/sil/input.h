/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef __SILHOUETTE_INPUT_H__
#define __SILHOUETTE_INPUT_H__

#include <gst/gst.h>

#include "stream.h"
#include "macros.h"

G_BEGIN_DECLS

/* SilInput */

#define SIL_TYPE_INPUT (sil_input_get_type ())
SIL_API
G_DECLARE_FINAL_TYPE (SilInput, sil_input, SIL, INPUT, SilStream)

SIL_API
SilInput * sil_input_new (const gchar * remote_name, const gchar * name,
    const gchar *link_group, const gchar *path);

SIL_API
void sil_input_set_appsrc (SilInput * self, GstElement *appsrc);

G_END_DECLS

#endif

