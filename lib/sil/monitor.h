/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef __SILHOUETTE_MONITOR_H__
#define __SILHOUETTE_MONITOR_H__

#include "pipewire-context.h"
#include "macros.h"

G_BEGIN_DECLS

/* SilMonitor */

#define SIL_TYPE_MONITOR (sil_monitor_get_type ())
SIL_API
G_DECLARE_FINAL_TYPE (SilMonitor, sil_monitor, SIL, MONITOR, SilPipewireContext)

SIL_API
SilMonitor * sil_monitor_new (const gchar *remote_name);

SIL_API
GArray * sil_monitor_get_source_ids (SilMonitor *self);

SIL_API
const gchar *sil_monitor_get_source_property (SilMonitor *self, guint source_id,
    const gchar *key);

G_END_DECLS

#endif

