/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef __SILHOUETTE_GTK_PLAYER_H__
#define __SILHOUETTE_GTK_PLAYER_H__

#include <gtk/gtk.h>
#include <sil/sil.h>

G_BEGIN_DECLS

/* SilGtkPlayer */

#define SIL_TYPE_GTK_PLAYER (sil_gtk_player_get_type ())
G_DECLARE_FINAL_TYPE (SilGtkPlayer, sil_gtk_player, SIL, GTK_PLAYER,
    SilPipeline)

SilGtkPlayer * sil_gtk_player_new (GMainContext *ctx, const gchar *remote_name,
    const gchar * name, const gchar *path);

GtkWidget * sil_gtk_player_get_widget (SilGtkPlayer *self);

G_END_DECLS

#endif
