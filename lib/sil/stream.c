/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include <unistd.h>

#include "logger.h"
#include "stream.h"

enum {
  STREAM_PROP_0,
  STREAM_PROP_NAME,
  STREAM_PROP_LINK_GROUP,
};

enum {
  STREAM_SIGNAL_ERROR,
  STREAM_SIGNAL_PAUSED,
  STREAM_SIGNAL_STREAMING,
  STREAM_NUM_SIGNALS
};

static guint32 signals [STREAM_NUM_SIGNALS];

typedef struct _SilStreamPrivate SilStreamPrivate;
struct _SilStreamPrivate
{
  /* Props */
  gint id;
  gchar *name;
  gchar *link_group;

  struct pw_stream *stream;
  struct spa_hook stream_listener;
};

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (SilStream, sil_stream,
    SIL_TYPE_PIPEWIRE_CONTEXT)

static gint
get_next_id ()
{
  static gint next_id = -1;
  g_atomic_int_inc (&next_id);
  return next_id;
}

static void
sil_stream_init (SilStream * self)
{
  SilStreamPrivate *priv = sil_stream_get_instance_private (self);
  priv->id = get_next_id ();
}

static void
on_process (void *data)
{
  SilStream *self = SIL_STREAM (data);
  SilStreamPrivate *priv = sil_stream_get_instance_private (self);
  struct pw_buffer *b;

  sil_pipewire_context_lock (SIL_PIPEWIRE_CONTEXT (self));

  /* Dequeue buffer */
  b = pw_stream_dequeue_buffer (priv->stream);
  if (!b) {
    sil_log_warn_object (self, "Out of buffers in stream %s: %m", priv->name);
    goto done;
  }

  /* Process */
  if (!SIL_STREAM_GET_CLASS (self)->process_buffer (self, b, priv->stream)) {
    sil_log_warn_object (self, "Failed to process buffer %p in stream %s", b,
        priv->name);
    goto done;
  }

done:
  /* Queue buffer */
  pw_stream_queue_buffer (priv->stream, b);
  sil_pipewire_context_unlock (SIL_PIPEWIRE_CONTEXT (self));
}

static void
on_stream_state_changed (void *data, enum pw_stream_state old,
    enum pw_stream_state state, const char *error)
{
  SilStream *self = SIL_STREAM (data);
  SilStreamPrivate *priv = sil_stream_get_instance_private (self);

  switch (state) {
    case PW_STREAM_STATE_ERROR:
      sil_log_error_object (self, "State changed to error on stream %s: %s",
          priv->name, error);
      g_signal_emit (self, signals[STREAM_SIGNAL_ERROR], 0, error,
          priv->stream);
      break;

    case PW_STREAM_STATE_PAUSED:
      sil_log_info_object (self, "State changed to paused on stream %s",
          priv->name);
      g_signal_emit (self, signals[STREAM_SIGNAL_PAUSED], 0, priv->stream);
      break;

    case PW_STREAM_STATE_STREAMING:
      sil_log_info_object (self, "State changed to streaming on stream %s",
          priv->name);
      g_signal_emit (self, signals[STREAM_SIGNAL_STREAMING], 0, priv->stream);
      break;

	default:
      break;
  }
}

static void
on_stream_param_changed (void *data, uint32_t id, const struct spa_pod *param)
{
  SilStream *self = SIL_STREAM (data);
  SilStreamPrivate *priv = sil_stream_get_instance_private (self);

  sil_log_info_object (self, "Param changed on stream %s: id=%d param=%p",
      priv->name, id, param);

  if (SIL_STREAM_GET_CLASS (self)->param_changed)
    SIL_STREAM_GET_CLASS (self)->param_changed (self, id, param, priv->stream);
}

static const struct pw_stream_events stream_events = {
  PW_VERSION_STREAM_EVENTS,
  .process = on_process,
  .state_changed = on_stream_state_changed,
  .param_changed = on_stream_param_changed,
};

static void
sil_stream_disconnect (SilPipewireContext *base)
{
  SilStream *self = SIL_STREAM (base);
  SilStreamPrivate *priv;

  priv = sil_stream_get_instance_private (self);

  g_clear_pointer (&priv->stream, pw_stream_destroy);
}

static gboolean
sil_stream_connect (SilPipewireContext *base, struct pw_core * core)
{
  SilStream *self = SIL_STREAM (base);
  SilStreamPrivate *priv;
  struct pw_properties *props;
  const gchar *str;

  g_return_val_if_fail (SIL_STREAM_GET_CLASS (self)->get_properties, FALSE);
  g_return_val_if_fail (SIL_STREAM_GET_CLASS (self)->connect_stream, FALSE);
  g_return_val_if_fail (SIL_STREAM_GET_CLASS (self)->process_buffer, FALSE);

  priv = sil_stream_get_instance_private (self);

  /* Get the stream properties */
  props = SIL_STREAM_GET_CLASS (self)->get_properties (self);

  /* Make sure name, description and link group are always set */
  str = pw_properties_get (props, PW_KEY_NODE_NAME);
  if (!str)
    pw_properties_set (props, PW_KEY_NODE_NAME, priv->name);
  str = pw_properties_get (props, PW_KEY_NODE_DESCRIPTION);
  if (!str)
    pw_properties_set (props, PW_KEY_NODE_DESCRIPTION, priv->name);
  str = pw_properties_get (props, PW_KEY_NODE_LINK_GROUP);
  if (!str)
    pw_properties_set (props, PW_KEY_NODE_LINK_GROUP, priv->link_group);

  /* Create the stream */
  priv->stream = pw_stream_new (core, priv->name, props);
  pw_stream_add_listener (priv->stream, &priv->stream_listener, &stream_events,
      self);

  /* Connect the stream */
  if (!SIL_STREAM_GET_CLASS (self)->connect_stream (self, priv->stream)) {
    sil_log_error_object (self, "Failed to connect stream %s", priv->name);
    goto error;
  }

  return TRUE;

error:
  sil_stream_disconnect (base);
  return FALSE;
}

static void
sil_stream_constructed (GObject *object)
{
  SilStream *self = SIL_STREAM (object);
  SilStreamPrivate *priv = sil_stream_get_instance_private (self);

  /* Make sure we always have a valid name */
  if (!priv->name)
    priv->name = g_strdup_printf ("stream-%d-%d", getpid (), priv->id);

  G_OBJECT_CLASS (sil_stream_parent_class)->constructed (object);
}

static void
sil_stream_set_property (GObject * object, guint property_id,
    const GValue * value, GParamSpec * pspec)
{
  SilStream *self = SIL_STREAM (object);
  SilStreamPrivate *priv = sil_stream_get_instance_private (self);

  switch (property_id) {
    case STREAM_PROP_NAME:
      g_clear_pointer (&priv->name, g_free);
      priv->name = g_value_dup_string (value);
      break;

    case STREAM_PROP_LINK_GROUP:
      g_clear_pointer (&priv->link_group, g_free);
      priv->link_group = g_value_dup_string (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

static void
sil_stream_get_property (GObject * object, guint property_id,
    GValue * value, GParamSpec * pspec)
{
  SilStream *self = SIL_STREAM (object);
  SilStreamPrivate *priv = sil_stream_get_instance_private (self);

  switch (property_id) {
    case STREAM_PROP_NAME:
      g_value_set_string (value, priv->name);
      break;

    case STREAM_PROP_LINK_GROUP:
      g_value_set_string (value, priv->link_group);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

static void
sil_stream_finalize (GObject * object)
{
  SilStream *self = SIL_STREAM (object);
  SilStreamPrivate *priv = sil_stream_get_instance_private (self);

  /* Props */
  g_clear_pointer (&priv->link_group, g_free);
  g_clear_pointer (&priv->name, g_free);

  G_OBJECT_CLASS (sil_stream_parent_class)->finalize (object);
}

static void
sil_stream_class_init (SilStreamClass * klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;
  SilPipewireContextClass *pc_class = (SilPipewireContextClass *) klass;

  object_class->constructed = sil_stream_constructed;
  object_class->finalize = sil_stream_finalize;
  object_class->set_property = sil_stream_set_property;
  object_class->get_property = sil_stream_get_property;

  pc_class->disconnect = sil_stream_disconnect;
  pc_class->connect = sil_stream_connect;

  g_object_class_install_property (object_class, STREAM_PROP_NAME,
      g_param_spec_string ("name", "name",
          "The name of the stream", NULL,
          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (object_class, STREAM_PROP_LINK_GROUP,
      g_param_spec_string ("link-group", "link-group",
          "The link group of the stream", NULL,
          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  signals[STREAM_SIGNAL_ERROR] = g_signal_new ("error",
      G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (SilStreamClass, error), NULL, NULL, NULL,
      G_TYPE_NONE, 2, G_TYPE_STRING, G_TYPE_POINTER);
  signals[STREAM_SIGNAL_PAUSED] = g_signal_new ("paused",
      G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (SilStreamClass, paused), NULL, NULL, NULL,
      G_TYPE_NONE, 1, G_TYPE_POINTER);
  signals[STREAM_SIGNAL_STREAMING] = g_signal_new ("streaming",
      G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (SilStreamClass, streaming), NULL, NULL, NULL,
      G_TYPE_NONE, 1, G_TYPE_POINTER);
}

const gchar *
sil_stream_get_name (SilStream * self)
{
  SilStreamPrivate *priv;

  g_return_val_if_fail (SIL_IS_STREAM (self), NULL);

  priv = sil_stream_get_instance_private (self);
  return priv->name;
}
