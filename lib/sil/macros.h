/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef __SILHOUETTE_DEFS_H__
#define __SILHOUETTE_DEFS_H__

#if defined(__GNUC__)
# define SIL_PLUGIN_EXPORT __attribute__ ((visibility ("default")))
# define SIL_API_EXPORT extern __attribute__ ((visibility ("default")))
#else
# define SIL_PLUGIN_EXPORT
# define SIL_API_EXPORT extern
#endif

#define SIL_API_IMPORT extern

#ifndef SIL_API
# ifdef BUILDING_SIL
#  define SIL_API SIL_API_EXPORT
# else
#  define SIL_API SIL_API_IMPORT
# endif
#endif

#ifndef SIL_PRIVATE_API
# ifdef BUILDING_SIL
#  define SIL_PRIVATE_API
# else
#  define SIL_PRIVATE_API __attribute__ ((deprecated ("Private API")))
# endif
#endif

#define SIL_PADDING(n) gpointer _sil_padding[n];

#endif
