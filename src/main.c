/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include <sil/sil.h>
#include <gtk/gtk.h>

#include "gtk-player.h"

static gboolean show_version_ = FALSE;
static gboolean list_filter_factories_ = FALSE;
static gint source_ = 0;

static GOptionEntry entries[] =
{
  { "version", 'v', G_OPTION_FLAG_NONE, G_OPTION_ARG_NONE, &show_version_,
      "Show version", NULL },
  { "list-filter-factories", 'l', G_OPTION_FLAG_NONE, G_OPTION_ARG_NONE,
      &list_filter_factories_, "List all the loaded filters factories", NULL },
  { "source", 's', G_OPTION_FLAG_NONE, G_OPTION_ARG_INT, &source_,
      "The PipeWire video source Id to use: 0 = Auto (Default: 0)", "ID" },
  { NULL }
};

struct _UserInterface {
  GtkWidget *window;
  GtkWidget *main_box;
  GtkWidget *top_status_box;
  GtkWidget *top_status_bar;
  GtkWidget *video_box;
  GtkWidget *video_widget;
  GtkWidget *status_box;
  GtkWidget *status_bar;
  GtkWidget *controls_box;
  GtkWidget *stop_button;
  GtkWidget *play_button;
  GHashTable *combos;
};
typedef struct _UserInterface UserInterface;

struct _ProgramData {
  gint done_seq;
  gboolean done;
  GMutex done_mutex;
  GCond done_cond;
  SilMonitor *monitor;
  SilChannel *channel;
  SilGtkPlayer *player;
  guint32 source_id;
  const gchar *source_name;
  UserInterface ui;
};
typedef struct _ProgramData ProgramData;

static void
program_data_init (ProgramData * self)
{
  self->done_seq = 0;
  self->done = FALSE;
  g_mutex_init (&self->done_mutex);
  g_cond_init (&self->done_cond);
  self->monitor = NULL;
  self->channel = NULL;
  self->player = NULL;
  self->source_id = 0;
  self->source_name = NULL;
  self->ui.combos = g_hash_table_new_full (g_direct_hash, g_direct_equal, NULL,
      NULL);
}

static void
program_data_clear (ProgramData * self)
{
  g_clear_object (&self->player);
  g_clear_object (&self->channel);
  g_clear_object (&self->monitor);
  g_cond_clear (&self->done_cond);
  g_mutex_clear (&self->done_mutex);
  g_clear_pointer (&self->ui.combos, g_hash_table_unref);
}

static void
wait_for_monitor_done (ProgramData *pd)
{
  pd->done = FALSE;
  pd->done_seq = sil_pipewire_context_sync (SIL_PIPEWIRE_CONTEXT (pd->monitor));
  g_mutex_lock (&pd->done_mutex);
  while (!pd->done)
    g_cond_wait (&pd->done_cond, &pd->done_mutex);
  g_mutex_unlock (&pd->done_mutex);
}

static void
trigger_monitor_done (ProgramData *pd)
{
  g_mutex_lock (&pd->done_mutex);
  pd->done = TRUE;
  g_cond_signal (&pd->done_cond);
  g_mutex_unlock (&pd->done_mutex);
}

G_DEFINE_AUTO_CLEANUP_CLEAR_FUNC (ProgramData, program_data_clear)

static void
print_version (const gchar *path)
{
  g_print ("%s\n"
      "Compiled with libsilhouette %s\n"
      "Linked with libsilhouette %s\n",
      path,
      SILHOUETTE_VERSION,
      sil_get_library_version ());
}

static void
print_filter_factories ()
{
  g_autoptr (SilFilterRegistry) registry = sil_get_filter_registry ();
  GList *list, *elem;

  g_print ("Silhouette Filter Factories:\n");

  list = sil_filter_registry_get_factories (registry);
  for (elem = list; elem; elem = elem->next) {
    SilFilterFactory *factory = elem->data;
    g_autofree gchar *factory_name = NULL;
    const gchar *desc;

    g_object_get (factory, "name", &factory_name, NULL);
    desc = sil_filter_factory_get_description (factory);

    g_print (" - %s: %s\n", factory_name, desc);
  }
  g_list_free (list);
}

static void
update_status_bar_message (GtkWidget *status_bar, const gchar *msg)
{
  guint id = gtk_statusbar_get_context_id (
      GTK_STATUSBAR (status_bar), "info");
  gtk_statusbar_pop (GTK_STATUSBAR (status_bar), id);
  gtk_statusbar_push (GTK_STATUSBAR (status_bar), id, msg);
}

static void
on_monitor_source_added (SilMonitor* monitor, guint id,
    struct pw_properties *props, gpointer data)
{
  const gchar *name = pw_properties_get (props, "node.name");
  sil_log_debug ("Source with Id %d added: %s", id, name);
}

static void
on_monitor_source_removed (SilMonitor* monitor, guint id,
    struct pw_properties *props, gpointer data)
{
  const gchar *name = pw_properties_get (props, "node.name");
  sil_log_debug ("Source with Id %d removed: %s", id, name);
}

static void
on_monitor_core_done (SilMonitor* monitor, guint id, gint seq, gpointer data)
{
  ProgramData *pd = (ProgramData *)data;

  if (pd->done_seq == seq)
    trigger_monitor_done (pd);
}

static guint32
get_max_properity_source (ProgramData *pd)
{
  gint max_prio = G_MININT;
  guint32 res_id = SPA_ID_INVALID;
  g_autoptr (GArray) source_ids = sil_monitor_get_source_ids (pd->monitor);

  for (guint i = 0; i < source_ids->len; i++) {
    guint32 id = g_array_index (source_ids, guint32, i);
    const gchar *prio_str = sil_monitor_get_source_property (pd->monitor,
        id, "priority.session");
    gint prio = prio_str ? atoi (prio_str) : G_MININT;
    if (prio >= max_prio) {
      max_prio = prio;
      res_id = id;
    }
  }

  return res_id;
}

static void
on_channel_error (SilChannel * p, const gchar *msg, ProgramData * pd)
{
  g_autofree gchar *status_msg = NULL;
  status_msg = g_strdup_printf ("Channel error: %s", msg);
  update_status_bar_message (pd->ui.status_bar, status_msg);
}

static void
on_channel_started (SilChannel * c, const gchar *msg, ProgramData * pd)
{
  update_status_bar_message (pd->ui.status_bar, "Starting player...");

  if (!sil_pipeline_play (SIL_PIPELINE (pd->player))) {
    update_status_bar_message (pd->ui.status_bar,
        "Could not start channel player");
    return;
  }
}

static void
on_player_error (SilGtkPlayer * p, const gchar *msg, ProgramData * pd)
{
  g_autofree gchar *status_msg = NULL;
  status_msg = g_strdup_printf ("Player error: %s", msg);
  update_status_bar_message (pd->ui.status_bar, status_msg);
}

static void
on_player_started (SilGtkPlayer * p, const gchar *msg, ProgramData * pd)
{
  update_status_bar_message (pd->ui.status_bar, "Playing...");

  gtk_widget_set_sensitive (pd->ui.stop_button, TRUE);
}

static void
on_main_window_delete_event (GtkWidget *widget, GdkEvent *event,
    ProgramData * pd) {
  gtk_main_quit ();
}

static void
on_stop_button_clicked (GtkWidget *widget, ProgramData * pd)
{
  gtk_widget_set_sensitive (pd->ui.stop_button, FALSE);
  gtk_widget_set_sensitive (pd->ui.play_button, TRUE);

  sil_pipeline_stop (SIL_PIPELINE (pd->player));
  sil_pipeline_stop (SIL_PIPELINE (pd->channel));

  update_status_bar_message (pd->ui.status_bar, "Stopped");
}

static void
on_play_button_clicked (GtkWidget *widget, ProgramData * pd)
{
  gtk_widget_set_sensitive (pd->ui.play_button, FALSE);

  update_status_bar_message (pd->ui.status_bar, "Starting channel...");

  sil_pipeline_play (SIL_PIPELINE (pd->channel));
}

static void
on_combo_changed (GtkComboBox * widget, ProgramData * pd)
{
  GtkWidget *label = g_hash_table_lookup (pd->ui.combos, widget);
  if (label) {
    g_autoptr (SilFilter) filter = sil_channel_get_filter (pd->channel);
    if (filter)
      g_object_set (filter, gtk_label_get_text (GTK_LABEL (label)),
          gtk_combo_box_get_active (widget), NULL);
  }
}

static void
create_ui (ProgramData *pd)
{
  g_autoptr (SilFilter) filter = NULL;
  g_autofree gchar *msg = NULL;

  /* Create main window */
  pd->ui.window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (pd->ui.window), "Silhouette");
  gtk_window_set_default_size (GTK_WINDOW (pd->ui.window), 640, 480);
  g_signal_connect (G_OBJECT (pd->ui.window), "delete-event",
      G_CALLBACK (on_main_window_delete_event), pd);

  /* Create the main box */
  pd->ui.main_box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
  gtk_container_add (GTK_CONTAINER (pd->ui.window), pd->ui.main_box);

  /* Create the top status box */
  pd->ui.top_status_box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
  gtk_widget_set_halign (pd->ui.top_status_box, GTK_ALIGN_CENTER);
  gtk_widget_set_valign (pd->ui.top_status_box, GTK_ALIGN_CENTER);
  gtk_box_pack_start (GTK_BOX (pd->ui.main_box), pd->ui.top_status_box, FALSE,
      FALSE, 0);

  /* Create the top status bar */
  pd->ui.top_status_bar = gtk_statusbar_new ();
  msg = g_strdup_printf ("Source %d | %s", pd->source_id, pd->source_name);
  update_status_bar_message (pd->ui.top_status_bar, msg);
  gtk_box_pack_start (GTK_BOX (pd->ui.top_status_box), pd->ui.top_status_bar,
      FALSE, FALSE, 0);

  /* Create the video box */
  pd->ui.video_box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
  gtk_widget_set_halign (pd->ui.video_box, GTK_ALIGN_FILL);
  gtk_widget_set_valign (pd->ui.video_box, GTK_ALIGN_FILL);
  gtk_box_pack_start (GTK_BOX (pd->ui.main_box), pd->ui.video_box, TRUE, TRUE,
      0);

  /* Add video widget to video box */
  gtk_box_pack_start (GTK_BOX (pd->ui.video_box), pd->ui.video_widget, TRUE,
      TRUE, 0);

  /* Create the status box */
  pd->ui.status_box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
  gtk_widget_set_halign (pd->ui.status_box, GTK_ALIGN_CENTER);
  gtk_widget_set_valign (pd->ui.status_box, GTK_ALIGN_CENTER);
  gtk_box_pack_start (GTK_BOX (pd->ui.main_box), pd->ui.status_box, FALSE,
      FALSE, 0);

  /* Create the status bar */
  pd->ui.status_bar = gtk_statusbar_new ();
  update_status_bar_message (pd->ui.status_bar, "Starting channel...");
  gtk_box_pack_start (GTK_BOX (pd->ui.status_box), pd->ui.status_bar, FALSE,
      FALSE, 0);

  /* Create the controls box */
  pd->ui.controls_box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
  gtk_widget_set_halign (pd->ui.controls_box, GTK_ALIGN_CENTER);
  gtk_widget_set_valign (pd->ui.controls_box, GTK_ALIGN_CENTER);
  gtk_box_pack_start (GTK_BOX (pd->ui.main_box), pd->ui.controls_box, FALSE,
      FALSE, 0);

  /* Create the stop button */
  pd->ui.stop_button = gtk_button_new_with_label ("Stop");
  g_signal_connect (pd->ui.stop_button, "clicked",
      G_CALLBACK (on_stop_button_clicked), pd);
  gtk_box_pack_start (GTK_BOX (pd->ui.controls_box), pd->ui.stop_button,
      FALSE, FALSE, 0);

  /* Create the play button */
  pd->ui.play_button = gtk_button_new_with_label ("Play");
  gtk_widget_set_sensitive (pd->ui.play_button, FALSE);
  g_signal_connect (pd->ui.play_button, "clicked",
      G_CALLBACK (on_play_button_clicked), pd);
  gtk_box_pack_start (GTK_BOX (pd->ui.controls_box), pd->ui.play_button,
      FALSE, FALSE, 0);

  /* Create a combo box for each filter property of type enum */
  filter = sil_channel_get_filter (pd->channel);
  if (filter) {
    GObjectClass *filter_class = NULL;
    GParamSpec  **props;
    guint n_properties;
    filter_class = G_OBJECT_GET_CLASS (filter);
    props = g_object_class_list_properties (filter_class, &n_properties);
    for (guint i = 0; i < n_properties; i ++) {
      GParamSpec *prop = props [i];
      guint j = 0;
      if (G_IS_PARAM_SPEC_ENUM (prop)) {
        const gchar *prop_name = g_param_spec_get_name (prop);
        guint prop_value = 0;
        GtkWidget *label, *combo;
        GEnumValue *values;

        combo = gtk_combo_box_text_new ();
        label = gtk_label_new (prop_name);
        g_hash_table_insert (pd->ui.combos, combo, label);

        /* Populate combo */
        values = G_ENUM_CLASS (g_type_class_ref (prop->value_type))->values;
        while (values[j].value_name) {
          gtk_combo_box_text_append (GTK_COMBO_BOX_TEXT (combo), NULL,
              values[j].value_nick);
          j++;
        }

        g_object_get (filter, prop_name, &prop_value, NULL);
        gtk_combo_box_set_active (GTK_COMBO_BOX (combo), prop_value);
        g_signal_connect (combo, "changed", G_CALLBACK (on_combo_changed), pd);
        gtk_box_pack_start (GTK_BOX (pd->ui.controls_box), label, FALSE, FALSE,
            5);
        gtk_box_pack_start (GTK_BOX (pd->ui.controls_box), combo, FALSE, FALSE,
            0);
      }
    }
    g_free (props);
  }

  /* Present the main window */
  gtk_widget_show_all (pd->ui.window);
}

gint
main (gint argc, gchar *argv[])
{
  g_auto (ProgramData) pd = { 0, };
  g_autoptr (GOptionContext) context = NULL;
  g_autoptr (GError) error = NULL;
  gint res = -1;

  /* Init GTK */
  gtk_init (NULL, NULL);

  /* Init Silhouette */
  sil_init (SIL_LOGGER_LEVEL_WARN);

  /* Init Program Data */
  program_data_init (&pd);

  /* Parse flags */
  context = g_option_context_new ("- Silhouette");
  g_option_context_add_main_entries (context, entries, NULL);
  if (!g_option_context_parse (context, &argc, &argv, &error)) {
    g_printerr ("%s\n", error->message);
    goto done;
  }
  if (source_ < 0) {
    g_printerr ("Source Id must be greater than or equal to 0\n");
    goto done;
  }

  /* Handle flags */
  if (show_version_)
    print_version (argv[0]);
  if (list_filter_factories_)
    print_filter_factories ();
  if (show_version_ || list_filter_factories_) {
    res = 0;
    goto done;
  }

  /* Create the monitor */
  pd.monitor = sil_monitor_new (NULL);
  g_signal_connect (pd.monitor, "source-added",
      G_CALLBACK (on_monitor_source_added), &pd);
  g_signal_connect (pd.monitor, "source-removed",
      G_CALLBACK (on_monitor_source_removed), &pd);
  g_signal_connect (pd.monitor, "core-done",
      G_CALLBACK (on_monitor_core_done), &pd);
  if (!sil_pipewire_context_connect (SIL_PIPEWIRE_CONTEXT (pd.monitor))) {
    g_printerr ("Could not connect monitor\n");
    goto done;
  }

  /* Wait for the monitor to finish scanning all sources */
  wait_for_monitor_done (&pd);

  /* Get max priority source if Id was not provided */
  if (source_ > 0) {
    pd.source_id = source_;
  } else {
    pd.source_id = get_max_properity_source (&pd);
    if (pd.source_id == SPA_ID_INVALID) {
      g_printerr ("Could not find any valid video source\n");
      goto done;
    }
  }

  /* Get source name */
  pd.source_name = sil_monitor_get_source_property (pd.monitor, pd.source_id,
      "node.name");
  if (!pd.source_name) {
    g_printerr ("Could not find video source with Id %d\n", pd.source_id);
    goto done;
  }

  /* Create the channel */
  pd.channel = sil_channel_new (NULL, NULL, NULL, "background", pd.source_name);
  g_signal_connect (pd.channel, "error", G_CALLBACK (on_channel_error), &pd);
  g_signal_connect (pd.channel, "started", G_CALLBACK (on_channel_started),
      &pd);

  /* Create the player */
  pd.player = sil_gtk_player_new (NULL, NULL, NULL,
      sil_channel_get_source_name (pd.channel));
  g_signal_connect (pd.player, "error", G_CALLBACK (on_player_error), &pd);
  g_signal_connect (pd.player, "started", G_CALLBACK (on_player_started), &pd);

  /* Get the playe rwidget */
  pd.ui.video_widget = sil_gtk_player_get_widget (pd.player);
  if (!pd.ui.video_widget) {
    g_printerr ("Could not get player widget\n");
    goto done;
  }

  /* Create the UI */
  create_ui (&pd);

  /* Play the channel */
  sil_pipeline_play (SIL_PIPELINE (pd.channel));

  /* Run */
  gtk_main ();
  res = 0;

done:
  return res;
}
