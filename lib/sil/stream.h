/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef __SILHOUETTE_STREAM_H__
#define __SILHOUETTE_STREAM_H__

#include "pipewire-context.h"
#include "macros.h"

G_BEGIN_DECLS

/* SilStream */

#define SIL_TYPE_STREAM (sil_stream_get_type ())
SIL_API
G_DECLARE_DERIVABLE_TYPE (SilStream, sil_stream, SIL, STREAM,
    SilPipewireContext)

struct _SilStreamClass
{
  SilPipewireContextClass parent_class;

  /* Signals */
  void (*error) (SilStream * self, const gchar *message,
      struct pw_stream *stream);
  void (*paused) (SilStream * self, struct pw_stream *stream);
  void (*streaming) (SilStream * self, struct pw_stream *stream);

  /* Virtual methods */
  void (*param_changed) (SilStream * self, guint32 id,
      const struct spa_pod *param, struct pw_stream *stream);
  struct pw_properties *(*get_properties) (SilStream * self);
  gboolean (*connect_stream) (SilStream * self, struct pw_stream *stream);
  gboolean (*process_buffer) (SilStream * self, struct pw_buffer *buffer,
      struct pw_stream *stream);
};

SIL_API
const gchar * sil_stream_get_name (SilStream * self);

G_END_DECLS

#endif

