/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include <spa/param/video/format-utils.h>
#include <pipewire/pipewire.h>
#include <gst/video/video-format.h>

#include "logger.h"
#include "source.h"

#define MAX_QUEUE_SIZE 4

#define DEFAULT_FORMAT GST_VIDEO_FORMAT_RGBA

enum {
  SOURCE_PROP_0,
  SOURCE_PROP_FORMAT,
  SOURCE_PROP_WIDTH,
  SOURCE_PROP_HEIGHT,
  SOURCE_PROP_FPS_N,
  SOURCE_PROP_FPS_D,
};

struct _SilSource
{
  SilStream parent;

  /* Props */
  GstVideoFormat format;
  guint width;
  guint height;
  guint fps_n;
  guint fps_d;

  struct spa_video_info_raw spa_format;
  GQueue *buffer_queue;
};

G_DEFINE_TYPE (SilSource, sil_source, SIL_TYPE_STREAM)

static enum spa_video_format
parse_gst_video_format (GstVideoFormat format)
{
  /* Only support RGB formats for now */
  switch (format) {
    case GST_VIDEO_FORMAT_RGB:
      return SPA_VIDEO_FORMAT_RGB;
    case GST_VIDEO_FORMAT_BGR:
      return SPA_VIDEO_FORMAT_BGR;
    case GST_VIDEO_FORMAT_RGBx:
      return SPA_VIDEO_FORMAT_RGBx;
    case GST_VIDEO_FORMAT_BGRx:
      return SPA_VIDEO_FORMAT_BGRx;
    case GST_VIDEO_FORMAT_xRGB:
      return SPA_VIDEO_FORMAT_xRGB;
    case GST_VIDEO_FORMAT_xBGR:
      return SPA_VIDEO_FORMAT_xBGR;
    case GST_VIDEO_FORMAT_RGBA:
      return SPA_VIDEO_FORMAT_RGBA;
    case GST_VIDEO_FORMAT_BGRA:
      return SPA_VIDEO_FORMAT_BGRA;
    case GST_VIDEO_FORMAT_ARGB:
      return SPA_VIDEO_FORMAT_ARGB;
    case GST_VIDEO_FORMAT_ABGR:
      return SPA_VIDEO_FORMAT_ABGR;
    default:
      break;
  }

  return SPA_VIDEO_FORMAT_UNKNOWN;
}

static guint
get_bits_per_pixel (GstVideoFormat format)
{
  /* Only support RGB formats for now */
  switch (format) {
    case GST_VIDEO_FORMAT_RGB:
    case GST_VIDEO_FORMAT_BGR:
      return 24;
    case GST_VIDEO_FORMAT_RGBx:
    case GST_VIDEO_FORMAT_BGRx:
    case GST_VIDEO_FORMAT_xRGB:
    case GST_VIDEO_FORMAT_xBGR:
    case GST_VIDEO_FORMAT_RGBA:
    case GST_VIDEO_FORMAT_BGRA:
    case GST_VIDEO_FORMAT_ARGB:
    case GST_VIDEO_FORMAT_ABGR:
      return 32;
    default:
      break;
  }

  return 0;
}

static void
sil_source_param_changed (SilStream * base, guint32 id,
    const struct spa_pod *param, struct pw_stream *stream)
{
  SilSource *self = SIL_SOURCE (base);
  uint8_t params_buffer[1024];
  struct spa_pod_builder b = SPA_POD_BUILDER_INIT (params_buffer,
      sizeof (params_buffer));
  const struct spa_pod *params[2];

  /* Make sure param is valid */
  if (!param)
    return;

  /* SPA_PARAM_Format */
  if (id == SPA_PARAM_Format) {
    guint bpp;
    gint stride;

    if (spa_format_video_raw_parse (param, &self->spa_format) < 0) {
      sil_log_warn_object (self, "Failed to parse format on source %s",
          sil_stream_get_name (base));
      return;
    }

    bpp = get_bits_per_pixel (self->spa_format.format);
    stride = self->spa_format.size.width * bpp / 8;

    params[0] = spa_pod_builder_add_object (&b,
        SPA_TYPE_OBJECT_ParamBuffers, SPA_PARAM_Buffers,
        SPA_PARAM_BUFFERS_buffers, SPA_POD_CHOICE_RANGE_Int (8, 2, 64),
        SPA_PARAM_BUFFERS_blocks, SPA_POD_Int (1),
        SPA_PARAM_BUFFERS_size, SPA_POD_Int (
            stride * self->spa_format.size.height),
        SPA_PARAM_BUFFERS_stride,  SPA_POD_Int (stride),
        SPA_PARAM_BUFFERS_dataType, SPA_POD_CHOICE_FLAGS_Int(
            (1<<SPA_DATA_MemPtr)));

    params[1] = spa_pod_builder_add_object (&b,
        SPA_TYPE_OBJECT_ParamMeta, SPA_PARAM_Meta,
        SPA_PARAM_META_type, SPA_POD_Id (SPA_META_Header),
        SPA_PARAM_META_size, SPA_POD_Int (sizeof(struct spa_meta_header)));

    sil_log_info_object (self,
        "Format changed on source %s: format=%d stride=%d width=%d height=%d",
        sil_stream_get_name (base), self->spa_format.format, stride,
        self->spa_format.size.width, self->spa_format.size.height);

    pw_stream_update_params (stream, params, 2);
  }
}

static struct pw_properties *
sil_source_get_properties (SilStream * base)
{
  return pw_properties_new (
      PW_KEY_MEDIA_TYPE, "Video",
      PW_KEY_MEDIA_CLASS, "Video/Source",
      PW_KEY_MEDIA_ROLE, "Camera",
      PW_KEY_STREAM_IS_LIVE, "true",
      NULL);
}

static gboolean
sil_source_connect_stream (SilStream * base, struct pw_stream *stream)
{
  SilSource *self = SIL_SOURCE (base);
  enum spa_video_format format;
  const struct spa_pod *params[1];
  guint8 buffer[1024];
  struct spa_pod_builder b = SPA_POD_BUILDER_INIT(buffer, sizeof(buffer));

  /* Parse GStreamer format */
  format = parse_gst_video_format (self->format);
  if (format == SPA_VIDEO_FORMAT_UNKNOWN) {
    sil_log_error_object (self, "Format '%s' is not supported on source %s",
        sil_stream_get_name (base), gst_video_format_to_string (self->format));
    return FALSE;
  }

  /* Connect the stream */
  params[0] = spa_pod_builder_add_object(&b,
      SPA_TYPE_OBJECT_Format, SPA_PARAM_EnumFormat,
      SPA_FORMAT_mediaType, SPA_POD_Id (SPA_MEDIA_TYPE_video),
      SPA_FORMAT_mediaSubtype, SPA_POD_Id (SPA_MEDIA_SUBTYPE_raw),
      SPA_FORMAT_VIDEO_format, SPA_POD_Id (format),
      SPA_FORMAT_VIDEO_size, SPA_POD_CHOICE_RANGE_Rectangle (
          &SPA_RECTANGLE (self->width, self->height),
          &SPA_RECTANGLE (1, 1),
          &SPA_RECTANGLE (G_MAXUINT, G_MAXUINT)),
      SPA_FORMAT_VIDEO_framerate, SPA_POD_Fraction (
          &SPA_FRACTION (self->fps_n, self->fps_d)));
  if (pw_stream_connect (stream, PW_DIRECTION_OUTPUT, PW_ID_ANY,
      PW_STREAM_FLAG_AUTOCONNECT |
      PW_STREAM_FLAG_ASYNC |
      PW_STREAM_FLAG_MAP_BUFFERS,
      params, 1) < 0) {
    sil_log_error_object (self, "Failed to connect source %s",
        sil_stream_get_name (base));
    return FALSE;
  }

  sil_log_info_object (self, "Successfully connected source %s",
      sil_stream_get_name (base));
  return TRUE;
}

static gboolean
sil_source_process_buffer (SilStream * base, struct pw_buffer *b,
    struct pw_stream *stream)
{
  SilSource *self = SIL_SOURCE (base);
  struct spa_meta_header *h;
  GstMapInfo map = { 0, };
  GstBuffer *gst_buffer = NULL;
  gboolean unref_buffer = FALSE;
  guint bpp;
  gint stride;

  /* Get GStreamer buffer */
  if (g_queue_get_length (self->buffer_queue) > 1) {
    gst_buffer = g_queue_pop_head (self->buffer_queue);
    unref_buffer = TRUE;
  } else {
    gst_buffer = g_queue_peek_head (self->buffer_queue);
    unref_buffer = FALSE;
  }
  if (!gst_buffer) {
    sil_log_info_object (self,
        "No GStreamer buffers when processing source %s",
        sil_stream_get_name (base));
    goto done;
  }

  /* Map GStreamer buffer */
  if (!gst_buffer_map (gst_buffer, &map, GST_MAP_READ)) {
    sil_log_info_object (self,
        "Failed to map GStreamer buffer %p when processing source %s",
        gst_buffer, sil_stream_get_name (base));
    goto done;
  }

  sil_log_trace_object (self,
      "Processing buffer %p in source %s: pts=%lu size=%d maxsize=%d",
      gst_buffer, sil_stream_get_name (base), gst_buffer->pts, map.size,
      map.maxsize);

  /* Check size */
  bpp = get_bits_per_pixel (self->spa_format.format);
  stride = SPA_ROUND_UP_N (self->spa_format.size.width * bpp / 8, 4);
  if (map.size != stride * self->spa_format.size.height) {
    sil_log_warn_object (self,
        "GStreamer buffer size does not match buffer in source %s: %lu != %lu",
        sil_stream_get_name (base), map.size,
        stride * self->spa_format.size.height);
    goto done;
  }

  /* Set header */
  h = spa_buffer_find_meta_data(b->buffer, SPA_META_Header, sizeof(*h));
  if (h)
    h->pts = pw_stream_get_nsec (stream);

  /* Copy */
  memcpy (b->buffer->datas[0].data, map.data, map.size);

  b->buffer->datas[0].maxsize = map.maxsize;
  b->buffer->datas[0].chunk->offset = 0;
  b->buffer->datas[0].chunk->size = stride * self->spa_format.size.height;
  b->buffer->datas[0].chunk->stride = stride;

done:
  if (map.data)
    gst_buffer_unmap (gst_buffer, &map);
  if (gst_buffer && unref_buffer)
    gst_buffer_unref (gst_buffer);

  return TRUE;
}

static void
sil_source_init (SilSource * self)
{
  self->format = DEFAULT_FORMAT;
  self->width = SIL_SOURCE_DEFAULT_WIDTH;
  self->height = SIL_SOURCE_DEFAULT_HEIGHT;
  self->fps_n = SIL_SOURCE_DEFAULT_FPS_N;
  self->fps_d = SIL_SOURCE_DEFAULT_FPS_D;

  self->buffer_queue = g_queue_new ();
}

static void
sil_source_set_property (GObject * object, guint property_id,
    const GValue * value, GParamSpec * pspec)
{
  SilSource *self = SIL_SOURCE (object);

  switch (property_id) {
    case SOURCE_PROP_FORMAT:
      self->format = g_value_get_enum (value);
      break;

    case SOURCE_PROP_WIDTH:
      self->width = g_value_get_uint (value);
      break;

    case SOURCE_PROP_HEIGHT:
      self->height = g_value_get_uint (value);
      break;

    case SOURCE_PROP_FPS_N:
      self->fps_n = g_value_get_uint (value);
      break;

    case SOURCE_PROP_FPS_D:
      self->fps_d = g_value_get_uint (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

static void
sil_source_get_property (GObject * object, guint property_id,
    GValue * value, GParamSpec * pspec)
{
  SilSource *self = SIL_SOURCE (object);

  switch (property_id) {
    case SOURCE_PROP_FORMAT:
      g_value_set_enum (value, self->format);
      break;

    case SOURCE_PROP_WIDTH:
      g_value_set_uint (value, self->width);
      break;

    case SOURCE_PROP_HEIGHT:
      g_value_set_uint (value, self->height);
      break;

    case SOURCE_PROP_FPS_N:
      g_value_set_uint (value, self->fps_n);
      break;

    case SOURCE_PROP_FPS_D:
      g_value_set_uint (value, self->fps_d);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

static void
sil_source_finalize (GObject * object)
{
  SilSource *self = SIL_SOURCE (object);

  /* Free queue */
  if (self->buffer_queue) {
    g_queue_free_full (self->buffer_queue, (GDestroyNotify) gst_buffer_unref);
    self->buffer_queue = NULL;
  }

  G_OBJECT_CLASS (sil_source_parent_class)->finalize (object);
}

static void
sil_source_class_init (SilSourceClass * klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;
  SilStreamClass *stream_class = (SilStreamClass *) klass;

  object_class->finalize = sil_source_finalize;
  object_class->set_property = sil_source_set_property;
  object_class->get_property = sil_source_get_property;

  stream_class->param_changed = sil_source_param_changed;
  stream_class->get_properties = sil_source_get_properties;
  stream_class->connect_stream = sil_source_connect_stream;
  stream_class->process_buffer = sil_source_process_buffer;

  g_object_class_install_property (object_class, SOURCE_PROP_FORMAT,
      g_param_spec_enum ("format", "format",
          "The raw video format of the source",
          GST_TYPE_VIDEO_FORMAT, DEFAULT_FORMAT,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (object_class, SOURCE_PROP_WIDTH,
      g_param_spec_uint ("width", "width",
          "The width of the source",
          1, G_MAXUINT, SIL_SOURCE_DEFAULT_WIDTH,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (object_class, SOURCE_PROP_HEIGHT,
      g_param_spec_uint ("height", "height",
          "The height of the source",
          1, G_MAXUINT, SIL_SOURCE_DEFAULT_HEIGHT,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (object_class, SOURCE_PROP_FPS_N,
      g_param_spec_uint ("fps-n", "fps-n",
          "The FPS numerator of the source",
          0, G_MAXUINT, SIL_SOURCE_DEFAULT_FPS_N,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (object_class, SOURCE_PROP_FPS_D,
      g_param_spec_uint ("fps-d", "fps-d",
          "The FPS denominator of the source",
          1, G_MAXUINT, SIL_SOURCE_DEFAULT_FPS_D,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
}

SilSource *
sil_source_new (const gchar *remote_name, const gchar * name,
    const gchar *link_group)
{
  return g_object_new (SIL_TYPE_SOURCE,
      "remote-name", remote_name,
      "name", name,
      "link-group", link_group,
      NULL);
}

void
sil_source_queue_buffer (SilSource *self, GstBuffer *buffer)
{
  g_return_if_fail (SIL_IS_SOURCE (self));
  g_return_if_fail (GST_IS_BUFFER (buffer));

  sil_pipewire_context_lock (SIL_PIPEWIRE_CONTEXT (self));

  /* Drop buffers if max length is reached */
  if (g_queue_get_length (self->buffer_queue) >= MAX_QUEUE_SIZE) {
    GstBuffer *head_buffer = g_queue_pop_head (self->buffer_queue);
    gst_buffer_unref (head_buffer);
  }

  /* Push buffer */
  g_queue_push_tail (self->buffer_queue, buffer);

  sil_pipewire_context_unlock (SIL_PIPEWIRE_CONTEXT (self));
}
