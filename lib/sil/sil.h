/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef __SILHOUETTE_SIL_H__
#define __SILHOUETTE_SIL_H__

#include "macros.h"
#include "silenums.h"
#include "silversion.h"
#include "logger.h"
#include "filter.h"
#include "pipeline.h"
#include "channel.h"
#include "pipewire-context.h"
#include "stream.h"
#include "source.h"
#include "input.h"
#include "monitor.h"

G_BEGIN_DECLS

SIL_API
void sil_init (SilLoggerLevel level);

SIL_API
void sil_deinit (void);

SIL_API
const gchar * sil_get_library_version (void);

SIL_API
const gchar * sil_get_library_api_version (void);

SIL_API
const gchar * sil_get_filter_dir (void);

SIL_API
const gchar * sil_get_data_dir (void);

SIL_API
SilFilterRegistry * sil_get_filter_registry (void);

G_END_DECLS

#endif

