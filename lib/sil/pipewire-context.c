/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include "logger.h"
#include "pipewire-context.h"

enum {
  PIPEWIRE_CONTEXT_PROP_0,
  PIPEWIRE_CONTEXT_PROP_REMOTE_NAME,
};

enum {
  PIPEWIRE_CONTEXT_SIGNAL_CORE_DONE,
  PIPEWIRE_CONTEXT_SIGNAL_CORE_ERROR,
  PIPEWIRE_CONTEXT_NUM_SIGNALS
};

static guint32 signals [PIPEWIRE_CONTEXT_NUM_SIGNALS];

typedef struct _SilPipewireContextPrivate SilPipewireContextPrivate;
struct _SilPipewireContextPrivate
{
  /* Props */
  gchar *remote_name;

  struct pw_thread_loop *loop;
  struct pw_context *context;
  struct pw_core *core;
  struct spa_hook core_listener;
};

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (SilPipewireContext, sil_pipewire_context,
    G_TYPE_OBJECT)

static void
sil_pipewire_context_init (SilPipewireContext * self)
{
}

static void
sil_pipewire_context_constructed (GObject *object)
{
  SilPipewireContext *self = SIL_PIPEWIRE_CONTEXT (object);
  SilPipewireContextPrivate *priv =
      sil_pipewire_context_get_instance_private (self);
  g_autofree gchar *thread_name = NULL;

  /* Create the pipewire thread loop */
  thread_name = g_strdup_printf ("pipewire-context-thread-loop.%p", self);
  priv->loop = pw_thread_loop_new (thread_name, NULL);

  /* Create the pipewire context */
  priv->context = pw_context_new (pw_thread_loop_get_loop (priv->loop), NULL,
      0);

  G_OBJECT_CLASS (sil_pipewire_context_parent_class)->constructed (object);
}

static void
sil_pipewire_context_set_property (GObject * object, guint property_id,
    const GValue * value, GParamSpec * pspec)
{
  SilPipewireContext *self = SIL_PIPEWIRE_CONTEXT (object);
  SilPipewireContextPrivate *priv =
      sil_pipewire_context_get_instance_private (self);

  switch (property_id) {
    case PIPEWIRE_CONTEXT_PROP_REMOTE_NAME:
      g_clear_pointer (&priv->remote_name, g_free);
      priv->remote_name = g_value_dup_string (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

static void
sil_pipewire_context_get_property (GObject * object, guint property_id,
    GValue * value, GParamSpec * pspec)
{
  SilPipewireContext *self = SIL_PIPEWIRE_CONTEXT (object);
  SilPipewireContextPrivate *priv =
      sil_pipewire_context_get_instance_private (self);

  switch (property_id) {
    case PIPEWIRE_CONTEXT_PROP_REMOTE_NAME:
      g_value_set_string (value, priv->remote_name);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

static void
sil_pipewire_context_dispose (GObject * object)
{
  SilPipewireContext *self = SIL_PIPEWIRE_CONTEXT (object);

  sil_pipewire_context_disconnect (self);
}

static void
sil_pipewire_context_finalize (GObject * object)
{
  SilPipewireContext *self = SIL_PIPEWIRE_CONTEXT (object);
  SilPipewireContextPrivate *priv =
      sil_pipewire_context_get_instance_private (self);

  g_clear_pointer (&priv->context, pw_context_destroy);
  g_clear_pointer (&priv->loop, pw_thread_loop_destroy);

  /* Props */
  g_clear_pointer (&priv->remote_name, g_free);

  G_OBJECT_CLASS (sil_pipewire_context_parent_class)->finalize (object);
}

static void
sil_pipewire_context_class_init (SilPipewireContextClass * klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  object_class->constructed = sil_pipewire_context_constructed;
  object_class->dispose = sil_pipewire_context_dispose;
  object_class->finalize = sil_pipewire_context_finalize;
  object_class->set_property = sil_pipewire_context_set_property;
  object_class->get_property = sil_pipewire_context_get_property;

  g_object_class_install_property (object_class,
      PIPEWIRE_CONTEXT_PROP_REMOTE_NAME,
      g_param_spec_string ("remote-name", "remote-name",
          "The name for the remote to connect to", NULL,
          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  signals[PIPEWIRE_CONTEXT_SIGNAL_CORE_DONE] = g_signal_new ("core-done",
      G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (SilPipewireContextClass, core_done), NULL, NULL, NULL,
      G_TYPE_NONE, 2, G_TYPE_UINT, G_TYPE_INT);

  signals[PIPEWIRE_CONTEXT_SIGNAL_CORE_ERROR] = g_signal_new ("core-error",
      G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST,
      G_STRUCT_OFFSET (SilPipewireContextClass, core_error), NULL, NULL, NULL,
      G_TYPE_NONE, 4, G_TYPE_UINT, G_TYPE_INT, G_TYPE_INT, G_TYPE_POINTER);
}

static void
on_core_done (void *data, uint32_t id, int seq)
{
  SilPipewireContext *self = SIL_PIPEWIRE_CONTEXT (data);

  sil_log_debug_object (self,
      "Pipewire context done received with id=%d and seq=%d", id, seq);

  g_signal_emit (self, signals[PIPEWIRE_CONTEXT_SIGNAL_CORE_DONE], 0, id, seq);
}

static void
on_core_error (void *data, uint32_t id, int seq, int res, const char *message)
{
  SilPipewireContext *self = SIL_PIPEWIRE_CONTEXT (data);

  sil_log_error_object (self,
      "Pipewire context error received with id=%d seq=%d and res=%d: %s",
      id, seq, res, message);

  g_signal_emit (self, signals[PIPEWIRE_CONTEXT_SIGNAL_CORE_ERROR], 0, id, seq,
      res, message);
}

static const struct pw_core_events core_events = {
    PW_VERSION_CORE_EVENTS,
    .done = on_core_done,
    .error = on_core_error,
};

gboolean
sil_pipewire_context_connect (SilPipewireContext *self)
{
  SilPipewireContextPrivate *priv;

  g_return_val_if_fail (SIL_IS_PIPEWIRE_CONTEXT (self), FALSE);

  priv = sil_pipewire_context_get_instance_private (self);

  g_return_val_if_fail (priv->loop, FALSE);

  /* Don't do anything if the context is already connected */
  if (priv->core)
    return TRUE;

  /* Connect to pipewire */
  priv->core = pw_context_connect (priv->context,
      pw_properties_new (PW_KEY_REMOTE_NAME, priv->remote_name, NULL), 0);
  if (!priv->core) {
    sil_log_error_object (self,
        "Failed to connect context, is PipeWire it running?: %m");
    goto error;
  }

  /* Add listener */
  pw_core_add_listener (priv->core, &priv->core_listener, &core_events, self);

  /* Call vmethod */
  if (SIL_PIPEWIRE_CONTEXT_GET_CLASS (self)->connect &&
      !SIL_PIPEWIRE_CONTEXT_GET_CLASS (self)->connect (self, priv->core))
    goto error;

  /* Start the pipewire thread loop */
  if (pw_thread_loop_start (priv->loop) < 0) {
    sil_log_error_object (self, "Failed to start thread loop: %m");
    goto error;
  }

  return TRUE;

error:
  sil_pipewire_context_disconnect (self);
  return FALSE;
}

void
sil_pipewire_context_disconnect (SilPipewireContext *self)
{
  SilPipewireContextPrivate *priv;

  g_return_if_fail (SIL_IS_PIPEWIRE_CONTEXT (self));

  priv = sil_pipewire_context_get_instance_private (self);

  /* Stop the pipewire thread loop */
  g_return_if_fail (priv->loop);
  pw_thread_loop_stop (priv->loop);

  /* Call vmethod */
  if (SIL_PIPEWIRE_CONTEXT_GET_CLASS (self)->disconnect)
    SIL_PIPEWIRE_CONTEXT_GET_CLASS (self)->disconnect (self);

  spa_hook_remove (&priv->core_listener);
  g_clear_pointer (&priv->core, pw_core_disconnect);
}

gboolean
sil_pipewire_context_is_connected (SilPipewireContext *self)
{
  SilPipewireContextPrivate *priv;

  g_return_val_if_fail (SIL_IS_PIPEWIRE_CONTEXT (self), FALSE);

  priv = sil_pipewire_context_get_instance_private (self);
  return priv->core != NULL;
}

gint
sil_pipewire_context_sync (SilPipewireContext *self)
{
  SilPipewireContextPrivate *priv;

  g_return_val_if_fail (SIL_IS_PIPEWIRE_CONTEXT (self), -1);

  priv = sil_pipewire_context_get_instance_private (self);

  g_return_val_if_fail (priv->core, -1);

  return pw_core_sync (priv->core, 0, 0);
}

void
sil_pipewire_context_lock (SilPipewireContext *self)
{
  SilPipewireContextPrivate *priv;

  g_return_if_fail (SIL_IS_PIPEWIRE_CONTEXT (self));

  priv = sil_pipewire_context_get_instance_private (self);

  g_return_if_fail (priv->loop);

  pw_thread_loop_lock (priv->loop);
}

void
sil_pipewire_context_unlock (SilPipewireContext *self)
{
  SilPipewireContextPrivate *priv;

  g_return_if_fail (SIL_IS_PIPEWIRE_CONTEXT (self));

  priv = sil_pipewire_context_get_instance_private (self);

  g_return_if_fail (priv->loop);
  pw_thread_loop_unlock (priv->loop);
}
