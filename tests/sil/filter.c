/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include <sil/sil.h>

static void
test_filter_registry (void)
{
  g_autoptr (SilFilterRegistry) r = NULL;
  GList *list, *elem;

  /* Get the registry */
  r = sil_get_filter_registry ();
  g_assert_nonnull (r);

  /* Get the factories */
  list = sil_filter_registry_get_factories (r);
  g_assert_nonnull (list);

  /* Make sure at least 1 factory was loaded */
  g_assert_cmpuint (g_list_length (list), >, 0);

  for (elem = list; elem; elem = elem->next) {
    SilFilterFactory *factory = elem->data;
    g_autofree gchar *factory_name = NULL;
    const gchar *desc;
    g_autoptr (SilFilter) f = NULL;
    g_autoptr (GstElement) e = NULL;

    /* Make sure the factory description is valid */
    g_object_get (factory, "name", &factory_name, NULL);
    desc = sil_filter_factory_get_description (factory);
    g_assert_nonnull (desc);

    /* Make sure we can create a filter using this factory */
    f = sil_filter_factory_make (factory_name, NULL);
    g_assert_nonnull (f);

    /* Make sure the filter has a valid GStreamer element */
    e = sil_filter_get_element (f);
    g_assert_nonnull (f);
  }

  g_list_free (list);
}

gint
main (gint argc, gchar *argv[])
{
  gint res;

  g_test_init (&argc, &argv, NULL);
  sil_init (SIL_LOGGER_LEVEL_TRACE);

  g_test_add_func ("/sil/filter/registry", test_filter_registry);

  res = g_test_run ();

  sil_deinit ();
  return res;
}
