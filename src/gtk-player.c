/* Silhouette
 *
 * Copyright © 2024 Collabora Ltd.
 *    @author Julian Bouzas <julian.bouzas@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include "gtk-player.h"

enum {
  GTK_PLAYER_PROP_0,
  GTK_PLAYER_PROP_REMOTE_NAME,
  GTK_PLAYER_PROP_PATH,
};

struct _SilGtkPlayer
{
  SilPipeline parent;

  /* Props */
  gchar *remote_name;
  gchar *path;

  SilInput *input;

  GstElement *appsrc;
  GstElement *videoconvert;
  GstElement *sink;
};

G_DEFINE_TYPE (SilGtkPlayer, sil_gtk_player, SIL_TYPE_PIPELINE)

static void
sil_gtk_player_init (SilGtkPlayer * self)
{
}

static gboolean
sil_gtk_player_build (SilPipeline * base, GstElement *gst_pipeline)
{
  SilGtkPlayer *self = SIL_GTK_PLAYER (base);

  /* Create elements */
  self->appsrc = gst_element_factory_make ("appsrc", NULL);
  if (!self->appsrc) {
    sil_log_error_object (self,
        "GTK Player '%s' cannot create appsrc element",
        sil_pipeline_get_name (base));
    goto error;
  }

  self->videoconvert = gst_element_factory_make ("videoconvert", NULL);
  if (!self->videoconvert) {
    sil_log_error_object (self,
        "GTK Player '%s' cannot create videoconvert element",
        sil_pipeline_get_name (base));
    goto error;
  }

  self->sink = gst_element_factory_make ("gtksink", NULL);
  if (!self->sink) {
    sil_log_error_object (self,
        "GTK Player '%s' cannot create sink element",
        sil_pipeline_get_name (base));
    goto error;
  }

  /* Configure appsrc */
  g_object_set (self->appsrc, "format", GST_FORMAT_TIME, NULL);
  g_object_set (self->appsrc, "do-timestamp", TRUE, NULL);

  /* Make appsrc receive buffers from input */
  sil_input_set_appsrc (self->input, self->appsrc);

  /* Configure gtksink */
  g_object_set (self->sink, "sync", FALSE, NULL);

  /* Add elements */
  gst_bin_add_many (GST_BIN (gst_pipeline),
      gst_object_ref (self->appsrc),
      gst_object_ref (self->videoconvert),
      gst_object_ref (self->sink),
      NULL);

  /* Link elements */
  if (!gst_element_link_many (self->appsrc, self->videoconvert,
      self->sink, NULL)) {
    gst_bin_remove_many (GST_BIN (gst_pipeline), self->appsrc,
        self->videoconvert, self->sink, NULL);
    sil_log_error_object (self, "GTK Player '%s' cannot link elements",
        sil_pipeline_get_name (base));
    goto error;
  }

  return TRUE;

error:
  gst_clear_object (&self->appsrc);
  gst_clear_object (&self->videoconvert);
  gst_clear_object (&self->sink);
  return FALSE;
}

static gboolean
sil_gtk_player_play (SilPipeline * base)
{
  SilGtkPlayer *self = SIL_GTK_PLAYER (base);

  /* Connect the input stream */
  if (!sil_pipewire_context_connect (SIL_PIPEWIRE_CONTEXT (self->input))) {
    sil_log_error_object (self,
        "Could not connect player '%s' input to PipeWire",
        sil_pipeline_get_name (base));
    return FALSE;
  }

  return TRUE;
}

static void
sil_gtk_player_stop (SilPipeline * base)
{
  SilGtkPlayer *self = SIL_GTK_PLAYER (base);

  /* Disconnect the input */
  sil_pipewire_context_disconnect (SIL_PIPEWIRE_CONTEXT (self->input));
}

static void
sil_gtk_player_constructed (GObject *object)
{
  SilGtkPlayer *self = SIL_GTK_PLAYER (object);
  g_autofree gchar *input_name = NULL;

  /* Make sure the name is set */
  G_OBJECT_CLASS (sil_gtk_player_parent_class)->constructed (object);

  /* Create the input */
  input_name = g_strdup_printf ("%s.input",
      sil_pipeline_get_name (SIL_PIPELINE (self)));
  self->input = sil_input_new (self->remote_name, input_name, NULL, self->path);
}

static void
sil_gtk_player_set_property (GObject * object, guint property_id,
    const GValue * value, GParamSpec * pspec)
{
  SilGtkPlayer *self = SIL_GTK_PLAYER (object);

  switch (property_id) {
    case GTK_PLAYER_PROP_REMOTE_NAME:
      g_clear_pointer (&self->remote_name, g_free);
      self->path = g_value_dup_string (value);
      break;

    case GTK_PLAYER_PROP_PATH:
      g_clear_pointer (&self->path, g_free);
      self->path = g_value_dup_string (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

static void
sil_gtk_player_get_property (GObject * object, guint property_id,
    GValue * value, GParamSpec * pspec)
{
  SilGtkPlayer *self = SIL_GTK_PLAYER (object);

  switch (property_id) {
    case GTK_PLAYER_PROP_REMOTE_NAME:
      g_value_set_string (value, self->remote_name);
      break;

    case GTK_PLAYER_PROP_PATH:
      g_value_set_string (value, self->path);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

static void
sil_gtk_player_finalize (GObject * object)
{
  SilGtkPlayer *self = SIL_GTK_PLAYER (object);

  gst_clear_object (&self->appsrc);
  gst_clear_object (&self->videoconvert);
  gst_clear_object (&self->sink);

  g_clear_object (&self->input);

  g_clear_pointer (&self->path, g_free);
  g_clear_pointer (&self->remote_name, g_free);

  G_OBJECT_CLASS (sil_gtk_player_parent_class)->finalize (object);
}

static void
sil_gtk_player_class_init (SilGtkPlayerClass * klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;
  SilPipelineClass *pipeline_class = (SilPipelineClass *) klass;

  object_class->constructed = sil_gtk_player_constructed;
  object_class->finalize = sil_gtk_player_finalize;
  object_class->set_property = sil_gtk_player_set_property;
  object_class->get_property = sil_gtk_player_get_property;

  pipeline_class->build = sil_gtk_player_build;
  pipeline_class->play = sil_gtk_player_play;
  pipeline_class->stop = sil_gtk_player_stop;

  g_object_class_install_property (object_class, GTK_PLAYER_PROP_REMOTE_NAME,
      g_param_spec_string ("remote-name", "remote-name",
          "The name for the remote to connect to", NULL,
          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (object_class, GTK_PLAYER_PROP_PATH,
      g_param_spec_string ("path", "path",
          "The pipewire path to capture video from", NULL,
          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));
}

SilGtkPlayer *
sil_gtk_player_new (GMainContext *ctx, const gchar *remote_name,
    const gchar * name, const gchar *path)
{
  return g_object_new (SIL_TYPE_GTK_PLAYER,
      "g-main-context", ctx,
      "remote-name", remote_name,
      "name", name,
      "path", path,
      NULL);
}

GtkWidget *
sil_gtk_player_get_widget (SilGtkPlayer *self)
{
  GtkWidget *res = NULL;

  g_return_val_if_fail (SIL_IS_GTK_PLAYER (self), NULL);

  if (!sil_pipeline_build (SIL_PIPELINE (self)))
    return FALSE;

  g_object_get (self->sink, "widget", &res, NULL);
  return res;
}
